﻿namespace Model.EF
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Ward")]
    public class Ward
    {
        [Key]
        public int ID { get; set; }

        [MaxLength(500)]
        public string WardName { get; set; }

        public int DistrictId { get; set; }

    }
}
