﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
using Model.ViewModel;
using PagedList;

namespace OnlineShop.Controllers
{
    public class ProductController : Controller
    {

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public PartialViewResult ProductCategory()
        {
            var model = new ProductCategoryDao().ListAll();
            return PartialView(model);
        }
        public JsonResult ListName(string q)
        {
            var data = new ProductDao().ListName(q);
            return Json(new
            {
                data = data,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Category(long cateId, int? page = 1, int? pageSize = 12)
        {
            var category = new CategoryDao().ViewDetail(cateId);
            ViewBag.Category = category;
            int totalRecord = 0;
            var model = new ProductDao().ListByCategoryId(cateId, ref totalRecord, 1, 12);
            ViewBag.CategoryID = cateId;
            ViewBag.Total = totalRecord;
            ViewBag.Page = page;

            int maxPage = 5;
            int totalPage = 0;

            totalPage = (int)Math.Ceiling((double)(totalRecord / pageSize));
            ViewBag.TotalPage = totalPage;
            ViewBag.MaxPage = maxPage;
            ViewBag.First = 1;
            ViewBag.Last = totalPage;
            ViewBag.Next = page + 1;
            ViewBag.Prev = page - 1;

            return View(model);
        }
        public ActionResult Search(string keyword, int page = 1, int pageSize = 10)
        {
            int totalRecord = 0;
            var model = new ProductDao().Search(keyword, ref totalRecord, page, pageSize);

            ViewBag.Total = totalRecord;
            ViewBag.Page = page;
            ViewBag.Keyword = keyword;
            int maxPage = 5;
            int totalPage = 0;

            totalPage = (int)Math.Ceiling((double)(totalRecord / pageSize));
            ViewBag.TotalPage = totalPage;
            ViewBag.MaxPage = maxPage;
            ViewBag.First = 1;
            ViewBag.Last = totalPage;
            ViewBag.Next = page + 1;
            ViewBag.Prev = page - 1;

            return View(model);
        }

        [OutputCache(CacheProfile = "Cache1DayForProduct")]
        public ActionResult Detail(long id)
        {
            var product = new ProductDao().ViewDetail(id);
            ViewBag.Category = new ProductCategoryDao().ViewDetail(product.CategoryID.Value);
            ViewBag.RelatedProducts = new ProductDao().ListRelatedProducts(id, 4);
            return View(product);
        }

        //public JsonResult ChangeProductColorId(int productColorId)
        //{
        //    var image = new ProductColorDao().GetImageByProductColorId(productColorId);
        //    return Json(image, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetAllListProduct(string price, string searchString, int categoryId = 0, int? page = 1, int pageSize = 12)
        {
            var list = new ProductDao().GetAllListProduct(page);            
            int pageNumber = (page ?? 1);
            List<SearchPrice> ListPrice = new List<SearchPrice>();
            string[] priceString;
            if (price != null)
            {
                priceString = price.Split(',');
                foreach (var item in priceString)
                {
                    var pricefrom = 0;
                    var priceto = 0;
                    if (item == "1")
                    {
                        priceto = 400000;
                    }
                    else if (item == "2")
                    {
                        pricefrom = 400000;
                        priceto = 800000;
                    }
                    else if (item == "3")
                    {
                        pricefrom = 800000;
                        priceto = 10000000;
                    }
                    var itemTemp = new SearchPrice();
                    itemTemp.id = item;
                    itemTemp.categoryId = "0";
                    itemTemp.priceFrom = pricefrom.ToString();
                    itemTemp.priceTo = priceto.ToString();
                    if (ListPrice.Count > 0)
                    {
                        for (var i = 0; i < ListPrice.Count; i++)
                        {
                            var itemPrice = ListPrice[i];
                            if (itemPrice.id != itemTemp.id)
                            {
                                ListPrice.Add(itemTemp);
                            }
                        }
                    }
                    else
                    {
                        ListPrice.Add(itemTemp);
                    }
                }
            }
            if (ListPrice != null && ListPrice.Count > 0)
            {
                var listProductTemp = new List<Product>();
                foreach (var item in ListPrice)
                {
                    var listItem = new ProductDao().GetProductByRangePrice(item.priceFrom, item.priceTo, item.categoryId, page);

                    if (listItem != null && listItem.Count > 0)
                    {
                        listProductTemp.AddRange(listItem); // AddRange định vị được vị trí thêm vào
                    }
                }
                list = listProductTemp;
            }
            else
            {
                list = new ProductDao().GetAllListProduct(page).ToList();
            }
            ViewBag.GetAllProduct = list.ToPagedList(pageNumber, pageSize);
            ViewBag.Price = price;
            
            return View();
        }
        //[HttpPost]
        //public ActionResult GetDataProduct(string price, int categoryId = 0, int? page = 1, int pageSize = 12)
        //{
        //    var list = new ProductDao().GetAllListProduct(page);            
        //    int pageNumber = (page ?? 1);
        //    List<SearchPrice> ListPrice = new List<SearchPrice>();
        //    string[] priceString;
        //    if (price != "")
        //    {
        //        priceString = price.Split(',');
        //        foreach (var item in priceString)
        //        {
        //            var pricefrom = 0;
        //            var priceto = 0;
        //            if (item == "1")
        //            {
        //                priceto = 400000;
        //            }
        //            else if (item == "2")
        //            {
        //                pricefrom = 400000;
        //                priceto = 800000;
        //            }
        //            else if (item == "3")
        //            {
        //                pricefrom = 800000;
        //                priceto = 10000000;
        //            }
        //            var itemTemp = new SearchPrice();
        //            itemTemp.id = item;
        //            itemTemp.categoryId = "0";
        //            itemTemp.priceFrom = pricefrom.ToString();
        //            itemTemp.priceTo = priceto.ToString();
        //            if (ListPrice.Count > 0)
        //            {
        //                for (var i = 0; i < ListPrice.Count; i++)
        //                {
        //                    var itemPrice = ListPrice[i];
        //                    if (itemPrice.id != itemTemp.id)
        //                    {
        //                        ListPrice.Add(itemTemp);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                ListPrice.Add(itemTemp);
        //            }
        //        }
        //    }
        //    if (ListPrice != null && ListPrice.Count > 0)
        //    {
        //        var listProductTemp = new List<Product>();
        //        foreach (var item in ListPrice)
        //        {
        //            var listItem = new ProductDao().GetProductByRangePrice(item.priceFrom, item.priceTo, item.categoryId, page);

        //            if (listItem != null && listItem.Count > 0)
        //            {
        //                listProductTemp.AddRange(listItem); // AddRange định vị được vị trí thêm vào
        //            }
        //        }
        //        list = listProductTemp;
        //    }
        //    else
        //    {
        //        list = new ProductDao().GetAllListProduct(page).ToList();
        //    }                           
        //    return PartialView("_GetListProductByPrice", list.ToPagedList(pageNumber, pageSize));
        //}
        [HttpGet]
        public JsonResult GetData(string price, int categoryId = 0, int? page = 1, int? pageSize = 12)
        {
            var list = new ProductDao().GetAllListProduct(page);            
            int start = (int)(page - 1) * (int)pageSize;
            // phan trang
            ViewBag.pageCurrent = page;
            
            List<SearchPrice> ListPrice = new List<SearchPrice>();
            string[] priceString;
            if (price != "")
            {
                priceString = price.Split(',');
                foreach (var item in priceString)
                {
                    if (!string.IsNullOrEmpty(item))
                    {                        
                        var pricefrom = 0;
                        var priceto = 0;
                        if (item == "1")
                        {
                            priceto = 400000;
                        }
                        else if (item == "2")
                        {
                            pricefrom = 400000;
                            priceto = 800000;
                        }
                        else if (item == "3")
                        {
                            pricefrom = 800000;
                            priceto = 10000000;
                        }
                        var itemTemp = new SearchPrice();
                        itemTemp.id = item;
                        itemTemp.categoryId = "0";
                        itemTemp.priceFrom = pricefrom.ToString();
                        itemTemp.priceTo = priceto.ToString();
                        if (ListPrice.Count > 0)
                        {
                            for (var i = 0; i < ListPrice.Count; i++)
                            {
                                var itemPrice = ListPrice[i];
                                if (itemPrice.id != itemTemp.id)
                                {
                                    ListPrice.Add(itemTemp);
                                }
                            }
                        }
                        else
                        {
                            ListPrice.Add(itemTemp);
                        }
                    }
                }
            }
            if (ListPrice != null && ListPrice.Count > 0)
            {
                var listProductTemp = new List<Product>();
                foreach (var item in ListPrice)
                {
                    var listItem = new ProductDao().GetProductByRangePrice(item.priceFrom, item.priceTo, item.categoryId, page);

                    if (listItem != null && listItem.Count > 0)
                    {
                        listProductTemp.AddRange(listItem); // AddRange định vị được vị trí thêm vào
                    }
                }
                //list = listProductTemp.Skip(start).Take((int)pageSize).ToList();
                list = listProductTemp;
            }
            else
            {
                list = new ProductDao().GetAllListProduct(page);
            }
            int totalPage = list.Count();
            float totalNumsize = (totalPage / (float)pageSize);
            int numSize = (int)Math.Ceiling(totalNumsize);
            ViewBag.numSize = numSize;
            return Json(new { data = list.Skip(start).Take((int)pageSize).ToList(), price = price, cateId = categoryId, pageCurrent = page, numSize = numSize  }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataProduct(string price, string searchString, int categoryId = 0, int? page = 1, int? pageSize = 12)
            {
            var list = new ProductDao().GetAllListProduct(page);
            int start = (int)(page - 1) * (int)pageSize;
            // phan trang
            ViewBag.pageCurrent = page;
            List<SearchPrice> ListPrice = new List<SearchPrice>();
            string[] priceString;            
            if (price != "")
            {
                //"1,2,3"
                priceString = price.Split(',');
                foreach (var item in priceString)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        var pricefrom = 0;
                        var priceto = 0;
                        if (item == "1")
                        {
                            priceto = 400000;
                        }
                        else if (item == "2")
                        {
                            pricefrom = 400000;
                            priceto = 800000;
                        }
                        else if (item == "3")
                        {
                            pricefrom = 800000;
                            priceto = 10000000;
                        }
                        var itemTemp = new SearchPrice();
                        itemTemp.id = item;
                        itemTemp.categoryId = "0";
                        itemTemp.searchString = searchString;
                        itemTemp.priceFrom = pricefrom.ToString();
                        itemTemp.priceTo = priceto.ToString();
                        if (ListPrice.Count > 0)
                        {
                            for (var i = 0; i < ListPrice.Count; i++)
                            {
                                var itemPrice = ListPrice[i];
                                if (itemPrice.id != itemTemp.id)
                                {
                                    ListPrice.Add(itemTemp);
                                }
                            }
                        }
                        else
                        {
                            ListPrice.Add(itemTemp);
                        }
                    }
                }
            }
            else
            {
                var itemTemp = new SearchPrice();
                //itemTemp.id = item;
                itemTemp.categoryId = "0";
                itemTemp.searchString = searchString;
                itemTemp.priceFrom = null;
                itemTemp.priceTo = null;
                if (ListPrice.Count > 0)
                {
                    for (var i = 0; i < ListPrice.Count; i++)
                    {
                        var itemPrice = ListPrice[i];
                        if (itemPrice.id != itemTemp.id)
                        {
                            ListPrice.Add(itemTemp);
                        }
                    }
                }
                else
                {
                    ListPrice.Add(itemTemp);
                }
            }
            if (ListPrice != null && ListPrice.Count > 0)
            {
                var listProductTemp = new List<Product>();
                foreach (var item in ListPrice)
                {
                    var listItem = new ProductDao().GetProductByRangePriceAndSearchString(item.priceFrom, item.priceTo, item.searchString, item.categoryId, page);

                    if (listItem != null && listItem.Count > 0)
                    {
                        listProductTemp.AddRange(listItem); // AddRange định vị được vị trí thêm vào
                    }
                }
                //list = listProductTemp.Skip(start).Take((int)pageSize).ToList();
                list = listProductTemp;
            }
            else
            {
                list = new ProductDao().GetAllListProduct(page);
            }
            int totalPage = list.Count();
            float totalNumsize = (totalPage / (float)pageSize);
            int numSize = (int)Math.Ceiling(totalNumsize);
            ViewBag.numSize = numSize;
            return Json(new { data = list.Skip(start).Take((int)pageSize).ToList(), price = price, cateId = categoryId, searchstring = searchString, pageCurrent = page, numSize = numSize }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CombinePrice(List<SearchPrice> listPrice, int? page)
        {
            var list = new List<Product>();
            int pageSize = 1;
            int pageNumber = 18;
            if (listPrice != null && listPrice.Count > 0)
            {
                foreach (var item in listPrice)
                {
                    var listItem = new ProductDao().GetProductByRangePrice(item.priceFrom, item.priceTo, item.categoryId, page);

                    if (listItem != null && listItem.Count > 0)
                    {
                        list.AddRange(listItem); // AddRange định vị được vị trí thêm vào
                    }
                }
            }
            else
            {
                list = new ProductDao().GetAllListProduct(page).ToList();
            }

            return PartialView("_GetListProductByPrice", list.ToPagedList(pageSize, pageNumber));
        }
    }
}