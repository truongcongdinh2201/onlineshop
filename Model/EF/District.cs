﻿namespace Model.EF
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("District")]

    public class District
    {
        [Key]
        public int ID { get; set; }

        [MaxLength(500)]
        public string DistrictName { get; set; }

        public int ProvinceId { get; set; }

    }
}
