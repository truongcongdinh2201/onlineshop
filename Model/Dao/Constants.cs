﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    class Constants
    {
        public struct StoreProcedures
        {
            public const string GetListColor = "usp_ProductColor_GetListColor";
            public const string GetListSize = "usp_ProductSize_GetListSize";
            public const string GetColorImage = "usp_ProductColor_GetImage";
            public const string GetForAdmin = "usp_OrderDetail_GetForAdmin";
            public const string GetListStatus = "usp_Order_GetListStatus";
            public const string GetCodeOrder = "Order_Gen_Id";
            public const string GetListProductForAdmin = "usp_Product_GetListProductForAdmin";
            public const string GetListCategoryName = "usp_ProductCategory_GetListCateName";
            public const string GetListProductSizeForAdmin = "usp_ProductSize_GetSizeForAdmin";
            public const string GetProductByRangePrice_1 = "usp_ProductPrice_RangePrice_1";
            public const string GetProductByRangePrice_2 = "usp_ProductPrice_RangePrice_2";
            public const string GetProductByRangePrice_3 = "usp_ProductPrice_RangePrice_3";
            public const string GetProductByRangePrice = "usp_ProductPrice_RangePrice";
            public const string GetProductByRangePriceAndSearchString = "usp_ProductPrice_SearchStringProduct";
            public const string ChartOrder = "usp_TotalOrder_Monthly";

            public const string SearchOrderByDate = "usp_Order_SearchByDate";

            public const string ExportOrderToExcel = "usp_Order_ExportToExcel";
            public const string GetEmail = "usp_Mail_GetEmail";
            public const string GetNewProduct = "usp_Product_GetNewProduct";
            public const string GetUserForAdmin = "usp_User_GetForAdmin";
            
        }
    }
}
