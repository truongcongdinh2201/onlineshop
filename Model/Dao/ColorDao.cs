﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class ColorDao
    {
        OnlineShopDbContext db = null;
        public ColorDao()
        {
            db = new OnlineShopDbContext();
        }

        //public List<Color> ListColor()
        //{
        //    return db.Colors.ToList();
        //}

        public string GetColorName(int colorId)
        {
            var item = db.Colors.Where(x => x.ColorId == colorId).FirstOrDefault();
            string result = null;
            if (item != null) result = item.ColorName;
            return result;
        }
    }
}
