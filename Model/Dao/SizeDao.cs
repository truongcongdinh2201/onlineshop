﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class SizeDao
    {
        OnlineShopDbContext db = null;
        public SizeDao()
        {
            db = new OnlineShopDbContext();
        }

        public List<Size> ListSize()
        {
            return db.Sizes.Where(x => x.Status == true).ToList();
        }

        public  string GetSizeName(int sizeId)
        {
            var item = db.Sizes.Where(x => x.SizeId == sizeId).FirstOrDefault();
            string result = null;
            if (item != null) result = item.SizeName;
            return result;
        }        
    }
}
