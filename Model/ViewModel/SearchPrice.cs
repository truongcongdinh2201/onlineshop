﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ViewModel
{
    public class SearchPrice
    {
        public string priceFrom { get; set; }
        public string priceTo { get; set; }
        public string categoryId { get; set; }
        public string searchString { get; set; }
        public string id { get; set; }
    }
}
