﻿using Model.Dao;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            //var chart = new ChartDao().ChartOrder();
            //var model = new ChartModel();
            //model.categories = 
            return View();
        }
        public JsonResult GetTotalOrderCurrentYear()
        {
            var listOrderByMonth = new ChartDao().ChartOrder();
            var chart = new ChartModel();
            if (listOrderByMonth != null && listOrderByMonth.Count > 0)
            {
                var listCategory = new List<string>();
                var listSerie = new List<Series>();
                var listTotal = new List<long>();
                foreach (var item in listOrderByMonth)
                {
                    listCategory.Add(item.Month.ToString());
                    listTotal.Add(item.Total.Value);
                }
                var obj = new Series();
                obj.name = "Order";
                obj.data = listTotal;
                listSerie.Add(obj);
                chart.categories = listCategory;
                chart.series = listSerie;
            }
            return Json(chart, JsonRequestBehavior.AllowGet);
        }
    }
}