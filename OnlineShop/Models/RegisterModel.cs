﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShop.Models
{
    public class RegisterModel
    {
        [Key]
        public long ID { set; get; }

        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "Yêu cầu nhập tên đăng nhập")]

        public string UserName { set; get; }

        [Display(Name = "Mật khẩu")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu ít nhất 6 ký tự.")]
        [Required(ErrorMessage = "Yêu cầu nhập mật khẩu")]
        public string Password { set; get; }

        [Display(Name = "Xác nhận mật khẩu")]
        [Compare("Password", ErrorMessage = "Xác nhận mật khẩu không đúng.")]
        public string ConfirmPassword { set; get; }

        [Display(Name = "Họ tên")]
        [Required(ErrorMessage = "Yêu cầu nhập họ tên")]
        public string Name { set; get; }

        [Required(ErrorMessage = "Yêu cầu nhập địa chỉ")]
        [Display(Name = "Địa chỉ")]
        public string Address { set; get; }

        [Required(ErrorMessage = "Yêu cầu nhập email")]
        [EmailAddress(ErrorMessage ="Yêu cầu nhập như ví dụ xxxx@gmail.com")]
        [Display(Name = "Email")]
        public string Email { set; get; }

        [Required(ErrorMessage = "Yêu cầu nhập số điện thoai")]
        [Display(Name = "Điện thoại")]
        public string Phone { set; get; }

        [Display(Name="Tỉnh/thành")]
        [NotMapped]
        public int ProvinceId { set; get; }

        [Display(Name = "Quận/Quyện")]
        [NotMapped]
        public int DistrictId { set; get; }
        [Display(Name = "Phường/Xã")]
        [NotMapped]
        public int WardId { set; get; }
    }
}