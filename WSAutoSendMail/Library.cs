﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSAutoSendMail
{
    class Library
    {
        public static void WriteErrorLog(String Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();

            }
            catch
            {

            }
        }

        public static string GetFolderGetFile()
        {
            return ConfigurationManager.AppSettings["FolderGetFile"] != null ? ConfigurationManager.AppSettings["FolderGetFile"] : "C:\\Logs";
        }
        public static string GetNewFolderSaveFile()
        {
            return ConfigurationManager.AppSettings["FolderSaveFile"] != null ? ConfigurationManager.AppSettings["FolderSaveFile"] : "C:\\Logs";
        }
    }
}
