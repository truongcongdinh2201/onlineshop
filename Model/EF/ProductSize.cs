﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.EF
{
    [Table("ProductSize")]
    public class ProductSize
    {
        [Key]        
        public int ProductSizeId { get; set; }

        public int ProductId { get; set; }

        public int SizeId { get; set; }
        public int Quantity { get; set; }

        public bool? Status { get; set; }

        [NotMapped]
        public string SizeName { get; set; }
        [NotMapped]
        public string Name { get; set; }        
    }
}
