﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WSAutoSendMail
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG            
            Service1 myService = new Service1();
            //call the start method -this will start the Timer.
            myService.Start();
            //Set the Thread to sleep
            Thread.Sleep(300000);
            myService.Stop();
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);       
#endif

        }
    }
}
