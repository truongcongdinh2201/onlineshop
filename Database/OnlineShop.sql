USE [OnlineShop]
GO
/****** Object:  StoredProcedure [dbo].[usp_TotalOrder_Monthly]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_TotalOrder_Monthly]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductSize_GetSizeForAdmin]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductSize_GetSizeForAdmin]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductSize_GetListSize]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductSize_GetListSize]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice_3]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductPrice_RangePrice_3]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice_2]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductPrice_RangePrice_2]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice_1]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductPrice_RangePrice_1]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductPrice_RangePrice]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductColor_GetListColor]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductColor_GetListColor]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductColor_GetImage]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductColor_GetImage]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductCategory_GetListCateName]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_ProductCategory_GetListCateName]
GO
/****** Object:  StoredProcedure [dbo].[usp_Product_GetListProductForAdmin]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_Product_GetListProductForAdmin]
GO
/****** Object:  StoredProcedure [dbo].[usp_OrderDetail_GetForAdmin]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_OrderDetail_GetForAdmin]
GO
/****** Object:  StoredProcedure [dbo].[usp_Order_GetListStatus]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[usp_Order_GetListStatus]
GO
/****** Object:  StoredProcedure [dbo].[Order_Gen_Id]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP PROCEDURE [dbo].[Order_Gen_Id]
GO
ALTER TABLE [dbo].[About] DROP CONSTRAINT [DF_About_Status]
GO
ALTER TABLE [dbo].[About] DROP CONSTRAINT [DF_About_CreatedDate]
GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[UserGroup]
GO
/****** Object:  Table [dbo].[User]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[User]
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Tag]
GO
/****** Object:  Table [dbo].[SystemConfig]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[SystemConfig]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Status]
GO
/****** Object:  Table [dbo].[Slide]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Slide]
GO
/****** Object:  Table [dbo].[Size]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Size]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Role]
GO
/****** Object:  Table [dbo].[ProductSize]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[ProductSize]
GO
/****** Object:  Table [dbo].[ProductColor]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[ProductColor]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[ProductCategory]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Product]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[OrderDetail]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Order]
GO
/****** Object:  Table [dbo].[MenuType]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[MenuType]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Menu]
GO
/****** Object:  Table [dbo].[Language]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Language]
GO
/****** Object:  Table [dbo].[Footer]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Footer]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Feedback]
GO
/****** Object:  Table [dbo].[Credential]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Credential]
GO
/****** Object:  Table [dbo].[ContentTag]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[ContentTag]
GO
/****** Object:  Table [dbo].[Content]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Content]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Contact]
GO
/****** Object:  Table [dbo].[Color]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Color]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[Category]
GO
/****** Object:  Table [dbo].[About]    Script Date: 9/28/2020 9:27:10 PM ******/
DROP TABLE [dbo].[About]
GO
/****** Object:  Table [dbo].[About]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[About](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[MetaTitle] [varchar](250) NULL,
	[Description] [nvarchar](500) NULL,
	[Image] [nvarchar](250) NULL,
	[Detail] [ntext] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[MetaKeywords] [nvarchar](250) NULL,
	[MetaDescriptions] [nchar](250) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_About] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[MetaTitle] [varchar](250) NULL,
	[ParentID] [bigint] NULL,
	[DisplayOrder] [int] NULL CONSTRAINT [DF_Category_DisplayOrder]  DEFAULT ((0)),
	[SeoTitle] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Category_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[MetaKeywords] [nvarchar](250) NULL,
	[MetaDescriptions] [nchar](250) NULL,
	[Status] [bit] NULL CONSTRAINT [DF_Category_Status]  DEFAULT ((1)),
	[ShowOnHome] [bit] NULL CONSTRAINT [DF_Category_ShowOnHome]  DEFAULT ((0)),
	[Language] [varchar](2) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Color]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Color](
	[ColorId] [int] IDENTITY(1,1) NOT NULL,
	[ColorName] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Color] PRIMARY KEY CLUSTERED 
(
	[ColorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contact]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [ntext] NULL,
	[Lat] [float] NULL,
	[Lng] [float] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Content]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Content](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[MetaTitle] [varchar](250) NULL,
	[Description] [nvarchar](500) NULL,
	[Image] [nvarchar](250) NULL,
	[CategoryID] [bigint] NULL,
	[Detail] [ntext] NULL,
	[Warranty] [int] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Content_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[MetaKeywords] [nvarchar](250) NULL,
	[MetaDescriptions] [nchar](250) NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Content_Status]  DEFAULT ((1)),
	[TopHot] [datetime] NULL,
	[ViewCount] [int] NULL CONSTRAINT [DF_Content_ViewCount]  DEFAULT ((0)),
	[Tags] [nvarchar](500) NULL,
	[Language] [varchar](2) NULL,
 CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContentTag]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentTag](
	[ContentID] [bigint] NOT NULL,
	[TagID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ContentTag] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC,
	[TagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Credential]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Credential](
	[UserGroupID] [varchar](20) NOT NULL,
	[RoleID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Credential] PRIMARY KEY CLUSTERED 
(
	[UserGroupID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Content] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Feedback_CreatedDate]  DEFAULT (getdate()),
	[Status] [bit] NULL,
 CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Footer]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Footer](
	[ID] [varchar](50) NOT NULL,
	[Content] [ntext] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Footer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Language]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Language](
	[ID] [varchar](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[IsDefault] [bit] NOT NULL CONSTRAINT [DF_Language_IsDefault]  DEFAULT ((0)),
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](50) NULL,
	[Link] [nvarchar](250) NULL,
	[DisplayOrder] [int] NULL,
	[Target] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	[TypeID] [int] NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuType]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_MenuType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Order](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CustomerID] [bigint] NULL,
	[ShipName] [nvarchar](50) NULL,
	[ShipMobile] [varchar](50) NULL,
	[ShipAddress] [nvarchar](50) NULL,
	[Province] [nvarchar](250) NULL,
	[District] [nvarchar](250) NULL,
	[ShipEmail] [nvarchar](50) NULL,
	[StatusId] [int] NOT NULL CONSTRAINT [DF_Order_StatusId]  DEFAULT ((1)),
	[OrderCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[ProductID] [bigint] NOT NULL,
	[OrderID] [bigint] NOT NULL,
	[Quantity] [int] NULL CONSTRAINT [DF_OrderDetail_Quantity]  DEFAULT ((1)),
	[Price] [decimal](18, 0) NULL,
	[ColorId] [int] NOT NULL,
	[SizeId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Code] [varchar](10) NULL,
	[MetaTitle] [varchar](250) NULL,
	[Description] [nvarchar](500) NULL,
	[Image] [nvarchar](250) NULL,
	[ImageName] [nvarchar](250) NULL,
	[ImageNameDb] [nvarchar](250) NULL,
	[MoreImages] [xml] NULL,
	[Price] [decimal](18, 0) NULL CONSTRAINT [DF_Product_Price]  DEFAULT ((0)),
	[PromotionPrice] [decimal](18, 0) NULL,
	[IncludedVAT] [bit] NULL,
	[Quantity] [int] NOT NULL CONSTRAINT [DF_Product_Quantity]  DEFAULT ((0)),
	[CategoryID] [bigint] NULL,
	[Detail] [ntext] NULL,
	[Warranty] [int] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Product_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[MetaKeywords] [nvarchar](250) NULL,
	[MetaDescriptions] [nchar](250) NULL,
	[Status] [bit] NULL CONSTRAINT [DF_Product_Status]  DEFAULT ((1)),
	[TopHot] [datetime] NULL,
	[ViewCount] [int] NULL CONSTRAINT [DF_Product_ViewCount]  DEFAULT ((0)),
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[MetaTitle] [varchar](250) NULL,
	[ParentID] [bigint] NULL,
	[DisplayOrder] [int] NULL CONSTRAINT [DF_ProductCategory_DisplayOrder]  DEFAULT ((0)),
	[SeoTitle] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_ProductCategory_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[MetaKeywords] [nvarchar](250) NULL,
	[MetaDescriptions] [nchar](250) NULL,
	[Status] [bit] NULL CONSTRAINT [DF_ProductCategory_Status]  DEFAULT ((1)),
	[ShowOnHome] [bit] NULL CONSTRAINT [DF_ProductCategory_ShowOnHome]  DEFAULT ((0)),
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductColor]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductColor](
	[ProductColorId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[ColorId] [int] NOT NULL,
	[Status] [bit] NULL,
	[Image] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_ProductColor] PRIMARY KEY CLUSTERED 
(
	[ProductColorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductSize]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSize](
	[ProductSizeId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[SizeId] [int] NOT NULL,
	[Quantity] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_ProductSize] PRIMARY KEY CLUSTERED 
(
	[ProductSizeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Size]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Size](
	[SizeId] [int] IDENTITY(1,1) NOT NULL,
	[SizeName] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Size] PRIMARY KEY CLUSTERED 
(
	[SizeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Slide]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Slide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](250) NULL,
	[ImageName] [nvarchar](250) NULL,
	[ImageNameDb] [nvarchar](250) NULL,
	[DisplayOrder] [int] NULL CONSTRAINT [DF_Slide_DisplayOrder]  DEFAULT ((1)),
	[Link] [nvarchar](250) NULL,
	[Description] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Slide_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Slide] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL CONSTRAINT [DF_Status_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemConfig]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemConfig](
	[ID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Type] [varchar](50) NULL,
	[Value] [nvarchar](250) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag](
	[ID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Password] [varchar](32) NULL,
	[GroupID] [varchar](20) NULL CONSTRAINT [DF_User_GroupID]  DEFAULT ('MEMBER'),
	[Name] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[ProvinceID] [int] NULL,
	[DistrictID] [int] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_User_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserGroup](
	[ID] [varchar](20) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome], [Language]) VALUES (1, N'Tin thế giới', N'tin-the-gioi', NULL, 1, NULL, CAST(N'2015-08-15 07:49:20.183' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0, NULL)
INSERT [dbo].[Category] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome], [Language]) VALUES (2, N'Tin trong nước', N'tin-trong-nuoc', NULL, 2, NULL, CAST(N'2015-08-15 07:49:33.087' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0, NULL)
INSERT [dbo].[Category] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome], [Language]) VALUES (3, N'34234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'vi')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Color] ON 

INSERT [dbo].[Color] ([ColorId], [ColorName], [Status]) VALUES (1, N'Đỏ', 1)
INSERT [dbo].[Color] ([ColorId], [ColorName], [Status]) VALUES (2, N'Xanh', 1)
INSERT [dbo].[Color] ([ColorId], [ColorName], [Status]) VALUES (3, N'Hồng', 1)
INSERT [dbo].[Color] ([ColorId], [ColorName], [Status]) VALUES (4, N'Vàng', 1)
INSERT [dbo].[Color] ([ColorId], [ColorName], [Status]) VALUES (5, N'Cam', 1)
SET IDENTITY_INSERT [dbo].[Color] OFF
SET IDENTITY_INSERT [dbo].[Contact] ON 

INSERT [dbo].[Contact] ([ID], [Content], [Lat], [Lng], [Status]) VALUES (1, N'<p>Công ty CP Men Fashion</p><p>Địa chỉ: Số 1 An Dương Vương, Phường 7, Quận 6, TP Hồ Chí Minh </p> <p>Điện thoại: 0941 419 795</p>', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Contact] OFF
SET IDENTITY_INSERT [dbo].[Content] ON 

INSERT [dbo].[Content] ([ID], [Name], [MetaTitle], [Description], [Image], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount], [Tags], [Language]) VALUES (1, N'tin tức demo', N'tin-tuc-demo', N'424', N'/Data/images/14.PNG', 1, N'42342342', 12, CAST(N'2015-09-20 08:01:57.590' AS DateTime), N'toanbn', NULL, NULL, N'313', N'13                                                                                                                                                                                                                                                        ', 1, NULL, 0, N'tin tức,thời sự', NULL)
SET IDENTITY_INSERT [dbo].[Content] OFF
INSERT [dbo].[ContentTag] ([ContentID], [TagID]) VALUES (1, N'thoi-su')
INSERT [dbo].[ContentTag] ([ContentID], [TagID]) VALUES (1, N'tin-tuc')
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (1, N'43454', N'5345', N'53453', N'354', N'
       345                 ', CAST(N'2015-09-13 21:36:30.167' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (2, N'4234', N'4234', N'634', N'423', N'243    ', CAST(N'2015-09-13 21:37:45.667' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (3, N'r2', N'4234', N'43243', N'423', N'423
                        ', CAST(N'2015-09-13 21:38:27.120' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (4, N'Truong Cong Dinh', N'0941419795', N'dinhtcgcs16209@fpt.edu.vn', N'abccccc', N'Liên hệ gấp
                        ', CAST(N'2020-08-26 05:01:21.070' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (5, N'Truong Cong Dinh', N'0941419795', N'truongcongdinh2201@gmail.com', N'abccccc', N'âgagagagaga
                        ', CAST(N'2020-08-26 05:02:27.517' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (6, N'Võ Hoàng Thái', N'0941419795', N'truongcongdinh2201@gmail.com', N'abccccccccccc', N'AloAloAloAloAlo
                        ', CAST(N'2020-09-09 10:58:21.860' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (7, N'Truong Cong Dinh', N'0941419795', N'dinhtcgcs16209@fpt.edu.vn', N'abccccc', N'aaaaaaaaaaaa', CAST(N'2020-09-09 11:23:56.040' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (8, N'', N'', N'', N'', N'
                        ', CAST(N'2020-09-09 11:24:01.107' AS DateTime), NULL)
INSERT [dbo].[Feedback] ([ID], [Name], [Phone], [Email], [Address], [Content], [CreatedDate], [Status]) VALUES (9, N'Truong Cong Dinh', N'0941419795', N'dinhtcgcs16209@fpt.edu.vn', N'abccccc', N'a
                        ', CAST(N'2020-09-09 11:24:52.407' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Feedback] OFF
INSERT [dbo].[Footer] ([ID], [Content], [Status]) VALUES (N'footer', N'<div class="wrap">
    <div class="section group">
        <div class="col_1_of_4 span_1_of_4">
            <h4>Information</h4>
            <ul>
                <li><a href="about.html">About Us</a></li>
                <li><a href="contact.html">Customer Service</a></li>
                <li><a href="#">Advanced Search</a></li>
                <li><a href="delivery.html">Orders and Returns</a></li>
                <li><a href="contact.html">Contact Us</a></li>
            </ul>
        </div>        
        <div class="col_1_of_4 span_1_of_4">
            <h4>My account</h4>
            <ul>
                <li><a href="contact.html">Sign In</a></li>
                <li><a href="index.html">View Cart</a></li>
                <li><a href="#">My Wishlist</a></li>
                <li><a href="#">Track My Order</a></li>
                <li><a href="contact.html">Help</a></li>
            </ul>
        </div>
        <div class="col_1_of_4 span_1_of_4">
            <h4>Contact</h4>
            <ul>
                <li><span>+91-123-456789</span></li>
                <li><span>+00-123-000000</span></li>
            </ul>
            <div class="social-icons">
                <h4>Follow Us</h4>
                <ul>
                    <li><a href="#" target="_blank"><img src="images/facebook.png" alt="" /></a></li>
                    <li><a href="#" target="_blank"><img src="images/twitter.png" alt="" /></a></li>
                    <li><a href="#" target="_blank"><img src="images/skype.png" alt="" /> </a></li>
                    <li><a href="#" target="_blank"> <img src="images/dribbble.png" alt="" /></a></li>
                    <li><a href="#" target="_blank"> <img src="images/linkedin.png" alt="" /></a></li>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="copy_right">
    <p>© Copyright by MEN FASHION</a> </p>
</div>', 1)
INSERT [dbo].[Language] ([ID], [Name], [IsDefault]) VALUES (N'en', N'Tiếng Anh', 0)
INSERT [dbo].[Language] ([ID], [Name], [IsDefault]) VALUES (N'vi', N'Tiếng Việt', 1)
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([ID], [Text], [Link], [DisplayOrder], [Target], [Status], [TypeID]) VALUES (1, N'Trang chủ', N'/', 1, N'_blank', 1, 1)
INSERT [dbo].[Menu] ([ID], [Text], [Link], [DisplayOrder], [Target], [Status], [TypeID]) VALUES (2, N'Giới thiệu', N'/gioi-thieu', 2, N'_self', 1, 1)
INSERT [dbo].[Menu] ([ID], [Text], [Link], [DisplayOrder], [Target], [Status], [TypeID]) VALUES (3, N'Tin tức', N'/tin-tuc', 3, N'_self', 0, 1)
INSERT [dbo].[Menu] ([ID], [Text], [Link], [DisplayOrder], [Target], [Status], [TypeID]) VALUES (4, N'Sản phẩm', N'/san-pham', 4, N'_self', 1, 1)
INSERT [dbo].[Menu] ([ID], [Text], [Link], [DisplayOrder], [Target], [Status], [TypeID]) VALUES (5, N'Liên hệ', N'/lien-he', 5, N'_self', 1, 1)
INSERT [dbo].[Menu] ([ID], [Text], [Link], [DisplayOrder], [Target], [Status], [TypeID]) VALUES (6, N'Đăng nhập', N'/dang-nhap', 1, N'_self', 1, 2)
INSERT [dbo].[Menu] ([ID], [Text], [Link], [DisplayOrder], [Target], [Status], [TypeID]) VALUES (7, N'Đăng ký', N'/dang-ky', 2, N'_self', 1, 2)
SET IDENTITY_INSERT [dbo].[Menu] OFF
SET IDENTITY_INSERT [dbo].[MenuType] ON 

INSERT [dbo].[MenuType] ([ID], [Name]) VALUES (1, N'Menu chính')
INSERT [dbo].[MenuType] ([ID], [Name]) VALUES (2, N'Menu top')
SET IDENTITY_INSERT [dbo].[MenuType] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([ID], [CreatedDate], [CustomerID], [ShipName], [ShipMobile], [ShipAddress], [Province], [District], [ShipEmail], [StatusId], [OrderCode]) VALUES (1, CAST(N'2020-09-18 00:00:00.000' AS DateTime), NULL, N'abc', NULL, NULL, NULL, NULL, NULL, 2, NULL)
INSERT [dbo].[Order] ([ID], [CreatedDate], [CustomerID], [ShipName], [ShipMobile], [ShipAddress], [Province], [District], [ShipEmail], [StatusId], [OrderCode]) VALUES (2, CAST(N'2020-08-18 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Order] ([ID], [CreatedDate], [CustomerID], [ShipName], [ShipMobile], [ShipAddress], [Province], [District], [ShipEmail], [StatusId], [OrderCode]) VALUES (3, CAST(N'2020-08-18 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Order] ([ID], [CreatedDate], [CustomerID], [ShipName], [ShipMobile], [ShipAddress], [Province], [District], [ShipEmail], [StatusId], [OrderCode]) VALUES (4, CAST(N'2020-07-18 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL)
SET IDENTITY_INSERT [dbo].[Order] OFF
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (4, 15, 1, CAST(12535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (4, 16, 1, CAST(12535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 1, 3, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 2, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 3, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 4, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 5, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 6, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 11, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 13, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 17, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 18, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (5, 19, 1, CAST(22535000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 7, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 8, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 9, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 10, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 12, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 14, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 20, 5, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 21, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 21, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 21, 1, CAST(4300000 AS Decimal(18, 0)), 2, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 24, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 25, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 26, 1, CAST(4300000 AS Decimal(18, 0)), 2, 2)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 27, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 27, 1, CAST(4300000 AS Decimal(18, 0)), 2, 2)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 28, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 31, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 32, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 33, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 1, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 2, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 3, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 6, 1, CAST(4300000 AS Decimal(18, 0)), 0, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (21, 6, 1, CAST(121221 AS Decimal(18, 0)), 0, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 4, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
INSERT [dbo].[OrderDetail] ([ProductID], [OrderID], [Quantity], [Price], [ColorId], [SizeId]) VALUES (8, 5, 1, CAST(4300000 AS Decimal(18, 0)), 1, 1)
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (4, N'Áo Sơ Mi Hồng Có Túi', N'A04', N'ao-so-mi-nau-co-tui', N'Chất liệu Cotton', N'/assets/client/images/201904174.png', N'as5', N'201904174.png', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 111, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (5, N'Áo Sơ Mi Xanh Đậm Có Túi', N'A05', N'ao-so-mi-dam-co-tui', N'Chất liệu Cotton', N'/assets/client/images/202004365.jpg', N'as6', N'202004365.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 111, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (7, N'Quần Jeans Rách Gối', N'J01', N'quan-jean-rach-goi-cat-lai', N'Chất liệu tốt', N'/assets/client/images/202704083.jpg', N'quan-jean-rach-goi-cat-lai-qj1671_2_small-14970-t', N'202704083.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (8, N'Quần Jeans Rách Gối Đen', N'J02', N'quan-jean-rach-goi-den', N'Chất liệu tốt', N'/assets/client/images/202804650.jpg', N'quan-jean-rach-goi-den-qj1395_2_small-7745-t', N'202804650.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (9, N'Quần Jeans Rách Gối Xanh Biển', N'J03', N'quan-jean-rach-mau-xanh-bien', N'Chất liệu tốt', N'/assets/client/images/202804925.jpg', N'quan-jean-rach-mau-xanh-bien-qj1661_2_small-14961-t', N'202804925.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (10, N'Quần Jeans Rách Gối Xanh Biển ', N'J04', N'quan-jean-rach-mau-xanh-bien', N'Chất liệu tốt', N'/assets/client/images/202904474.jpg', N'quan-jean-rach-mau-xanh-bien-qj1667_2_small-14968-t', N'202904474.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (11, N'Quần Jeans Rách Gối Xanh Biển ', N'J06', N'quan-jean-rach-mau-xanh-bien', N'Chất liệu tốt', N'/assets/client/images/203004738.png', N'quan-jean-rach-mau-xanh-bien-qj1667_2_small-14974-t', N'203004738.png', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (12, N'Quần Jeans Rách Gối Xám', N'J07', N'quan-jean-rach-mau-xanh-bien', N'Chất liệu tốt', N'/assets/client/images/203004733.jpg', N'quan-jean-rach-mau-xanh-bien-qj1667_small-14968-t', N'203004733.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (13, N'Áo Thun Cổ Tim Bo', N'TS01', N'ao-thun-co-tim-bo', N'Chất liệu tốt', N'/assets/client/images/203304189.jpg', N'ao-thun-co-tim-bo-at715_small-10418-t', N'203304189.jpg', NULL, CAST(200000 AS Decimal(18, 0)), CAST(200000 AS Decimal(18, 0)), 1, 12, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (14, N'Áo Thun Cổ Tim Bo Đen', N'TS02', N'ao-thun-co-tim-bo-den', N'Chất liệu tốt', N'/assets/client/images/203304186.jpg', N'ao-thun-co-tim-den-at715_small-8748-t', N'203304186.jpg', NULL, CAST(200000 AS Decimal(18, 0)), CAST(200000 AS Decimal(18, 0)), 1, 12, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (15, N'Áo Thun Cổ Tim Nâu', N'TS03', N'ao-thun-co-tim-nau', N'Chất liệu tốt', N'/assets/client/images/203404131.jpg', N'ao-thun-co-tim-nau-at715_small-10421-t', N'203404131.jpg', NULL, CAST(200000 AS Decimal(18, 0)), CAST(200000 AS Decimal(18, 0)), 1, 12, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (16, N'Áo Thun Cổ Tim Đỏ', N'TS04', N'ao-thun-co-tim-do', N'Chất liệu tốt', N'/assets/client/images/203404636.jpg', N'ao-thun-co-tim-do-at715_small-10419-t', N'203404636.jpg', NULL, CAST(200000 AS Decimal(18, 0)), CAST(200000 AS Decimal(18, 0)), 1, 12, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (17, N'Áo Thun Cổ Tim Xanh Biển', N'TS05', N'ao-thun-co-tim-xanh-bien', N'Chất liệu tốt', N'/assets/client/images/203504108.jpg', N'ao-thun-co-tim-xanh-bien-dam-at715_small-10424-t', N'203504108.jpg', NULL, CAST(200000 AS Decimal(18, 0)), CAST(200000 AS Decimal(18, 0)), 1, 12, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (18, N'Áo Thun Cổ Tim Xanh Bích', N'TS06', N'ao-thun-co-tim-xanh-bich', N'Chất liệu tốt', N'/assets/client/images/203504915.jpg', N'ao-thun-co-tim-xanh-bich-at715_small-8753-t', N'203504915.jpg', NULL, CAST(200000 AS Decimal(18, 0)), CAST(200000 AS Decimal(18, 0)), 1, 12, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (19, N'Giày Mọi Màu Xanh Đen', N'S01', N'giay-moi-mau-xanh-den', N'Chất liệu tốt', N'/assets/client/images/205004090.jpg', N'giay-moi-mau-xanh-den-g165_small-9267-t', N'205004090.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (20, N'Giày Mọi Màu Đen', N'S02', N'giay-moi-mau-den', N'Chất liệu tốt', N'/assets/client/images/205104904.jpg', N'giay-moi-xanh-den-g168_small-10261-t', N'205104904.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (21, N'Giày Thể Thao Đen', N'S03', N'giay-the-thao-den', N'Chất liệu tốt', N'/assets/client/images/205204363.jpg', N'giay-the-thao-den-g215_small-10621-t', N'205204363.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Product] ([ID], [Name], [Code], [MetaTitle], [Description], [Image], [ImageName], [ImageNameDb], [MoreImages], [Price], [PromotionPrice], [IncludedVAT], [Quantity], [CategoryID], [Detail], [Warranty], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [TopHot], [ViewCount]) VALUES (22, N'Giày Thể Thao Đỏ', N'S03', N'giay-the-thao-do', N'Chất liệu tốt', N'/assets/client/images/205204083.jpg', N'giay-the-thao-do-g210_2_small-10611-t', N'205204083.jpg', NULL, CAST(500000 AS Decimal(18, 0)), CAST(500000 AS Decimal(18, 0)), 1, 12, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[ProductCategory] ON 

INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (1, N'Hàng Mới', N'hang-moi', NULL, 0, NULL, CAST(N'2020-08-25 18:56:18.950' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (2, N'Áo Nam', N'ao-nam', NULL, 0, NULL, CAST(N'2020-08-25 18:56:27.597' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (3, N'Quần Nam', N'quan-nam', NULL, 0, NULL, CAST(N'2020-08-25 18:57:12.553' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (4, N'Phụ Kiện', N'phu-kien', NULL, 0, NULL, CAST(N'2020-08-25 18:57:29.847' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (5, N'Giày Nam', N'giay-nam', NULL, 0, NULL, CAST(N'2020-08-25 18:57:50.460' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (6, N'Khuyến Mãi', N'khuyen-mai', NULL, 0, NULL, CAST(N'2020-08-25 18:58:13.207' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (7, N'Áo Sơ Mi', N'ao-so-mi', 2, 0, NULL, CAST(N'2020-08-25 18:59:25.750' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (8, N'Áo Thun', N'ao-thun', 2, 0, NULL, CAST(N'2020-08-25 18:59:42.370' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (9, N'Quần Jean', N'quan-jean', 3, 0, NULL, CAST(N'2020-08-25 19:00:39.593' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
INSERT [dbo].[ProductCategory] ([ID], [Name], [MetaTitle], [ParentID], [DisplayOrder], [SeoTitle], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [MetaKeywords], [MetaDescriptions], [Status], [ShowOnHome]) VALUES (10, N'Ví Da', N'vi-da', 4, 0, NULL, CAST(N'2020-08-25 19:01:06.447' AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
SET IDENTITY_INSERT [dbo].[ProductColor] ON 

INSERT [dbo].[ProductColor] ([ProductColorId], [ProductId], [ColorId], [Status], [Image]) VALUES (1, 1, 1, 1, N'/assets/client/images/new-pic3.jpg')
INSERT [dbo].[ProductColor] ([ProductColorId], [ProductId], [ColorId], [Status], [Image]) VALUES (2, 1, 2, 1, N'/assets/client/images/new-pic3.jpg')
INSERT [dbo].[ProductColor] ([ProductColorId], [ProductId], [ColorId], [Status], [Image]) VALUES (3, 8, 1, 1, N'/assets/client/images/new-pic3.jpg')
INSERT [dbo].[ProductColor] ([ProductColorId], [ProductId], [ColorId], [Status], [Image]) VALUES (4, 8, 2, 1, N'/assets/client/images/about_img.jpg')
SET IDENTITY_INSERT [dbo].[ProductColor] OFF
SET IDENTITY_INSERT [dbo].[ProductSize] ON 

INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (1, 1, 1, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (2, 1, 2, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (3, 2, 1, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (4, 2, 2, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (5, 3, 1, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (6, 4, 1, 12, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (7, 5, 1, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (8, 7, 1, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (9, 8, 1, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (10, 13, 1, 100, NULL)
INSERT [dbo].[ProductSize] ([ProductSizeId], [ProductId], [SizeId], [Quantity], [Status]) VALUES (11, 3, 2, 12, NULL)
SET IDENTITY_INSERT [dbo].[ProductSize] OFF
INSERT [dbo].[Role] ([ID], [Name]) VALUES (N'ADD_CONTENT', N'Thêm tin tức')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (N'ADD_USER', N'Thêm user')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (N'DELETE_USER', N'Xoá user')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (N'EDIT_CONTENT', N'Sửa tin tức')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (N'EDIT_USER', N'Sửa user')
INSERT [dbo].[Role] ([ID], [Name]) VALUES (N'VIEW_USER', N'Xem danh sách user')
SET IDENTITY_INSERT [dbo].[Size] ON 

INSERT [dbo].[Size] ([SizeId], [SizeName], [Status]) VALUES (1, N'M', 1)
INSERT [dbo].[Size] ([SizeId], [SizeName], [Status]) VALUES (2, N'L', 1)
INSERT [dbo].[Size] ([SizeId], [SizeName], [Status]) VALUES (3, N'S', 1)
SET IDENTITY_INSERT [dbo].[Size] OFF
SET IDENTITY_INSERT [dbo].[Slide] ON 

INSERT [dbo].[Slide] ([ID], [Image], [ImageName], [ImageNameDb], [DisplayOrder], [Link], [Description], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status]) VALUES (1, N'/assets/client/images/slider_3.jpg', NULL, NULL, 1, N'/', N'Abccccccc', CAST(N'2015-08-26 19:21:44.440' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[Slide] ([ID], [Image], [ImageName], [ImageNameDb], [DisplayOrder], [Link], [Description], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status]) VALUES (2, N'/assets/client/images/slider_4.jpg', NULL, NULL, 2, N'/', NULL, CAST(N'2015-08-26 19:22:01.173' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[Slide] ([ID], [Image], [ImageName], [ImageNameDb], [DisplayOrder], [Link], [Description], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status]) VALUES (4, N'/assets/client/images/slider_2.jpg', NULL, NULL, 3, N'/', NULL, CAST(N'2020-09-17 20:09:06.987' AS DateTime), NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Slide] OFF
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([StatusId], [StatusName], [IsDeleted]) VALUES (1, N'Đang giao hàng', 0)
INSERT [dbo].[Status] ([StatusId], [StatusName], [IsDeleted]) VALUES (2, N'Hoàn thành', 0)
SET IDENTITY_INSERT [dbo].[Status] OFF
INSERT [dbo].[Tag] ([ID], [Name]) VALUES (N'thoi-su', N'thời sự')
INSERT [dbo].[Tag] ([ID], [Name]) VALUES (N'tin-tuc', N'tin tức')
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([ID], [UserName], [Password], [GroupID], [Name], [Address], [Email], [Phone], [ProvinceID], [DistrictID], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status]) VALUES (2, N'toanbn', N'202cb962ac59075b964b07152d234b70', N'MOD', N'toanf', N'hn', N'ngoctoan.dev@gmail.com', N'121', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[User] ([ID], [UserName], [Password], [GroupID], [Name], [Address], [Email], [Phone], [ProvinceID], [DistrictID], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status]) VALUES (15, N'toanbn1', N'202cb962ac59075b964b07152d234b70', N'MEMBER', N'toan', NULL, N'ngoctoan89112@gmail.com', N'121', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[User] ([ID], [UserName], [Password], [GroupID], [Name], [Address], [Email], [Phone], [ProvinceID], [DistrictID], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status]) VALUES (30, N'dinh', N'202cb962ac59075b964b07152d234b70', N'ADMIN', N'Truong cong dinh', N'abc', N'truongcongdinh2201@gmail.com', N'0941419795', 701, 70111, CAST(N'2020-09-04 13:07:28.227' AS DateTime), NULL, NULL, NULL, 1)
INSERT [dbo].[User] ([ID], [UserName], [Password], [GroupID], [Name], [Address], [Email], [Phone], [ProvinceID], [DistrictID], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status]) VALUES (31, N'dinh123', N'4297f44b13955235245b2497399d7a93', NULL, N'Truong Dinh', N'abbbbb', N'dinhtruong220198@gmail.com', N'12312387', 701, 70111, CAST(N'2020-09-27 19:15:34.050' AS DateTime), NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[User] OFF
INSERT [dbo].[UserGroup] ([ID], [Name]) VALUES (N'ADMIN', N'Quản trị')
INSERT [dbo].[UserGroup] ([ID], [Name]) VALUES (N'MEMBER', N'Thành viên')
INSERT [dbo].[UserGroup] ([ID], [Name]) VALUES (N'MOD', N'Moderatior')
ALTER TABLE [dbo].[About] ADD  CONSTRAINT [DF_About_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[About] ADD  CONSTRAINT [DF_About_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  StoredProcedure [dbo].[Order_Gen_Id]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Order_Gen_Id]
/*
*****************************************************************************************
* Description: Phát sinh mã phiếu chuyển/huỷ
*
* 
* Author				Version     Date            Content
*****************************************************************************************
* Nguyễn Xuân Thiên		1.00		2019-05-03		Tạo mới
*****************************************************************************************
*/
(
	@OrderCode varchar(15) out
)
as
begin
	declare @Year	   varchar(4)
	declare @Month	   varchar(2)
	declare @Day	   varchar(2)

	-- Ngày
	select @Year = CONVERT(varchar(4), GETDATE(), 120) 
	select @Month = SUBSTRING(CONVERT(varchar(19), GETDATE(), 120), 6, 2)
	select @Day = SUBSTRING(CONVERT(varchar(19), GETDATE(), 120), 9, 2)

	-- Max id
	declare @CurrentDate datetime
	set @CurrentDate = cast(@Year + '-' + @Month + '-' + @Day as datetime)
	--	>
	declare @MaxId int
	select  @MaxId = max(cast(substring(OrderCode, 12, 4) as bigint))
	from [Order]
	where CreatedDate > @CurrentDate
	--	>
	if @MaxId is null
		begin
			set @MaxId = 1
		end
	else
		begin
			set @MaxId = @MaxId + 1
		end

	-- Mã phiếu tồn kho
	set @OrderCode = @Year + @Month + @Day + 'ODE' + RIGHT('0000' + CAST(@MaxId AS VARCHAR), 4)
end


GO
/****** Object:  StoredProcedure [dbo].[usp_Order_GetListStatus]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Order_GetListStatus] 
AS
select [Order].*, [Status].StatusName from [Order]
left join [Status] on [Order].StatusId = [Status].StatusId

GO
/****** Object:  StoredProcedure [dbo].[usp_OrderDetail_GetForAdmin]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OrderDetail_GetForAdmin] 
@orderId int
AS
select OrderDetail.OrderID, Product.Name, OrderDetail.Price, OrderDetail.Quantity, Size.SizeName, Color.ColorName from OrderDetail
left join Product on OrderDetail.ProductID = Product.ID
left join Size on OrderDetail.SizeId = Size.SizeId
left join Color on OrderDetail.ColorId = Color.ColorId
where OrderID = @orderId
GO
/****** Object:  StoredProcedure [dbo].[usp_Product_GetListProductForAdmin]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Product_GetListProductForAdmin] 

AS

select ID,
	   Name, 
	   Code, 
	   MetaTitle, 
	   Description, 
	   Image, 
	   Price,
	   PromotionPrice, 
	   IncludedVAT, 
	   Quantity, 
	   Detail, 
	   Status, 
	   CategoryName,	   
	   LEFT(SizeName,Len(SizeName)-1) as 'SizeName'
from (
				select Product.ID,
					   Product.Name, 
					   Product.Code, 
					   Product.MetaTitle, 
					   Product.[Description], 
					   Product.[Image], 
					   Product.Price,
					   Product.PromotionPrice, 
					   Product.IncludedVAT, 
					   Product.Quantity, 
					   Product.Detail, 
					   Product.[Status], 
					   ProductCategory.Name as CategoryName,	   
					   (SELECT SUM(Quantity) FROM ProductSize where ProductId = Product.ID) as TotalQuantity,
					   (SELECT Size.SizeName + ',' AS [text()]
					   FROM ProductSize
						left join Size on ProductSize.SizeId = Size.SizeId
						WHERE ProductId = Product.ID for xml path ('')) as SizeName
				from Product
				left join ProductCategory on Product.CategoryID = ProductCategory.ID
				left join ProductSize on Product.ID = ProductSize.ProductId
				left join Size on ProductSize.SizeId = Size.SizeId 
) as Temp

GO
/****** Object:  StoredProcedure [dbo].[usp_ProductCategory_GetListCateName]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductCategory_GetListCateName] 
AS
select Product.*, ProductCategory.Name as CategoryName from Product
left join ProductCategory on Product.CategoryID = ProductCategory.ID

GO
/****** Object:  StoredProcedure [dbo].[usp_ProductColor_GetImage]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductColor_GetImage] 
@productColorId int
AS
select Image from ProductColor where ProductColorId = @productColorId

GO
/****** Object:  StoredProcedure [dbo].[usp_ProductColor_GetListColor]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_ProductColor_GetListColor] 
@productId int
AS
select ProductColor.*,Color.ColorName from ProductColor 
left join Color on ProductColor.ColorId=Color.ColorId
where ProductId= @productId


GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductPrice_RangePrice] (
@pricefrom decimal, @priceto decimal, @categoryId int)
AS
SELECT * from Product
where Price between @pricefrom and @priceto and (CategoryID = @categoryId or @categoryId = 0)
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice_1]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductPrice_RangePrice_1] 
AS 
SELECT * from Product
where Price <= 400000
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice_2]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductPrice_RangePrice_2] 
AS 
SELECT * from Product
where Price between 400000 and 800000
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductPrice_RangePrice_3]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductPrice_RangePrice_3] 
AS 
SELECT * from Product
where Price >= 800000
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductSize_GetListSize]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductSize_GetListSize] 
@productId int
AS
select ProductSize.*,Size.SizeName from ProductSize 
left join Size on ProductSize.SizeId=Size.SizeId
where ProductId= @productId

GO
/****** Object:  StoredProcedure [dbo].[usp_ProductSize_GetSizeForAdmin]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProductSize_GetSizeForAdmin] 
@productId int
AS
select ProductSize.ProductSizeId, Product.Name, Size.SizeName, ProductSize.Quantity, ProductSize.[Status] from ProductSize
left join Size on ProductSize.SizeId = Size.SizeId
left join Product on ProductSize.ProductId = Product.ID
where Product.ID = @productId
GO
/****** Object:  StoredProcedure [dbo].[usp_TotalOrder_Monthly]    Script Date: 9/28/2020 9:27:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TotalOrder_Monthly]
AS
select MONTH(CreatedDate) as Month,Convert(bigint,Count(ID)) as Total from [Order]
where YEAR(CreatedDate) =  YEAR(getdate())
group by MONTH(CreatedDate)
GO
