﻿var cart = {
    init: function () {
        cart.loadProvince();               
        cart.regEvents();        
    },

    loadProvince: function () {
        $.ajax({
            url: '/Cart/LoadProvince',
            type: "GET",
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    var html = '<option value="">--Chọn tỉnh thành--</option>';
                    var data = response.data;
                    console.log(data);
                    $.each(data, function (i, item) {
                        html += '<option value="' + item.ID + '">' + item.ProvinceName + '</option>'
                    });
                    $('#ddlProvince').html(html);
                }
            }
        })
    },
    loadDistrict: function (id) {
        $.ajax({
            url: '/Cart/LoadDistrict',
            data: { provinceId: id },
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    var html = '<option value="">--Chọn quận huyện--</option>';
                    var data = response.data;
                    console.log(data);
                    $.each(data, function (i, item) {
                        html += '<option value="' + item.ID + '">' + item.DistrictName + '</option>'
                    });
                    $('#ddlDistrict').html(html);
                }
            }
        })
    },
    loadWard: function (id) {
        $.ajax({
            url: '/Cart/LoadWard',
            data: { districtId: id },
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    var html = '<option value="">--Chọn phường/xã--</option>';
                    var data = response.data;
                    console.log(data);
                    $.each(data, function (i, item) {
                        html += '<option value="' + item.ID + '">' + item.WardName + '</option>'
                    });
                    $('#ddlWard').html(html);
                }
            }
        })
    },
    regEvents: function () {
        $('#btnContinue').off('click').on('click', function () {
            window.location.href = "/";
        });
        $('#btnPayment').off('click').on('click', function () {
            window.location.href = "/thanh-toan";
        });
        $('#btnUpdate').off('click').on('click', function () {
            var listProduct = $('.txtQuantity');
            var cartList = [];
            $.each(listProduct, function (i, item) {
                cartList.push({
                    Quantity: $(item).val(),
                    Product: {
                        ID: $(item).data('id')
                    }
                });
            });

            $.ajax({
                url: '/Cart/Update',
                data: { cartModel: JSON.stringify(cartList) },
                dataType: 'json',
                type: 'POST',
                success: function (res) {
                    if (res.status == true) {
                        window.location.href = "/gio-hang";
                    }
                }
            })
        });

        $('#btnDeleteAll').off('click').on('click', function () {


            $.ajax({
                url: '/Cart/DeleteAll',
                dataType: 'json',
                type: 'POST',
                success: function (res) {
                    if (res.status == true) {
                        window.location.href = "/gio-hang";
                    }
                }
            })
        });

        $('.btn-delete').off('click').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                data: { id: $(this).data('id') },
                url: '/Cart/Delete',
                dataType: 'json',
                type: 'POST',
                success: function (res) {
                    if (res.status == true) {
                        window.location.href = "/gio-hang";
                    }
                }
            })
        });

        $('#ddlProvince').off('change').on('change', function () {
            var id = $(this).val();
            if (id != '' && id > 0) {
                cart.loadDistrict(parseInt(id));
            }
            else {
                $('#ddlDistrict').html('');
            }
        });
        $('#ddlDistrict').off('change').on('change', function () {
            var id = $(this).val();
            if (id != '' && id > 0) {
                cart.loadWard(parseInt(id));
            }
            else {
                $('#ddlWard').html('');
            }
        });
    }
}
cart.init();