USE [OnlineShop]
GO
/****** Object:  StoredProcedure [dbo].[usp_Order_SearchByDate]    Script Date: 10/7/2020 11:13:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Order_SearchByDate] 
@fromDate datetime = null
, @toDate datetime = null
, @statusId int = null
, @searchString nvarchar(100)
AS
SET @todate = DATEADD(SECOND, -1, DATEADD(DAY, 1, @todate))
SELECT [Order].*, [Status].StatusName from [Order]
left join [Status] on [Order].StatusId = [Status].StatusId
	WHERE ([Order].StatusId = @statusId or @statusId = 0) 
		AND ((@fromDate is not null and @toDate is not null and [Order].CreatedDate BETWEEN @fromDate AND @toDate) 
			  OR (@fromDate is not null and  @toDate is null and   [Order].CreatedDate >= @fromDate) 
			  OR (@fromDate is null and  @toDate is not null and   [Order].CreatedDate <= @toDate) 
			  OR (@fromDate is null and  @toDate is null))
			  And (([Order].ShipName like '%'+@searchString +'%' or [Order].ShipMobile like '%'+@searchString +'%') or @searchString = '')
	ORDER BY [Order].CreatedDate DESC
GO
