﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ViewModel
{
    [NotMapped]
    public class ProductViewForAdmin: Product
    {
        public bool? IsDeteled { get; set; }
    }
}
