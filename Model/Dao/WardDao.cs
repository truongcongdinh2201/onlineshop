﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    
    public class WardDao
    {
        OnlineShopDbContext db = null;
        public WardDao()
        {
            db = new OnlineShopDbContext();
        }
        public List<Ward> GetAllListWard()
        {
            var list = db.Wards.ToList();
            return list;
        }

        public List<Ward> GetWardByDistrictId(int districtId)
        {
            var list = new List<Ward>();
            try
            {
                list = db.Wards.Where(x => x.DistrictId == districtId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }            
            return list;
        }

        public long Insert(Ward ward)
        {
            try
            {
                db.Wards.Add(ward);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return ward.ID;
        }
        public bool InsertAllWard(List<Ward> listWard)
        {
            bool result = false;
            try
            {
                db.Wards.AddRange(listWard);
                db.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public string GetWardName(int? wardId)
        {
            var item = db.Wards.Where(x => x.ID == wardId).FirstOrDefault();
            string result = null;
            if (item != null) result = item.WardName;
            return result;
        }
    }
}
