﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
namespace OnlineShop.Areas.Admin.Controllers
{
    public class FeedbackController : Controller
    {
        // GET: Admin/Feedback
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var model = new FeedbackDao().ListAllPagingFeedBack(searchString, page, pageSize);
            ViewBag.SearchString = searchString;

            return View(model);
        }
    }
}