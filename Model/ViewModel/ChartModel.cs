﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ViewModel
{
    public class ChartModel
    {
        public List<string> categories { get; set; }

         public List<Series> series { get; set; }
    }

    public class Series
    {
        public string name { get; set; }
        public List<long> data { get; set; }
    }
}
