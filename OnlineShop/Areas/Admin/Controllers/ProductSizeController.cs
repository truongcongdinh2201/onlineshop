﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
namespace OnlineShop.Areas.Admin.Controllers
{
    public class ProductSizeController : BaseController
    {
        // GET: Admin/ProductSize
        public ActionResult Index()
        {
            return View();
        }

       public JsonResult Delete(int id)
        {
            var isDeleted = new ProductSizeDao().Delete(id);
            return Json(isDeleted, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddOrUpdate(ProductSize proSize)
        {
            bool rs = false;
            if (proSize != null && proSize.ProductSizeId > 0)
            {
                var isUpdated = new ProductSizeDao().Update(proSize);
                if (isUpdated)
                {
                    rs = true;
                }
            }
            else
            {
                var proSizeId = new ProductSizeDao().Insert(proSize);
                if (proSizeId > 0)
                {
                    proSize.ProductSizeId = Convert.ToInt32(proSizeId);
                    rs = true;
                }
            }
            var data = new { isSuccess = rs, item = proSize };


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductSizeById(int id)
        {
            var item = new ProductSizeDao().GetProductSizeById(id);
            return Json(item, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListSize()
        {
            var list = new SizeDao().ListSize();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}