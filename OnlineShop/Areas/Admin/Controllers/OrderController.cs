﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
using Model.ViewModel;
using OfficeOpenXml;
using System.Drawing;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class OrderController : BaseController
    {
        // GET: Admin/Order
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var model = new OrderDao().ListAllOrderPagingForAdmin(searchString, page, pageSize);
            ViewBag.SearchString = searchString;
            ViewBag.ListStatus = new StatusDao().GetListStatus();
            return View(model);
        }

        public ActionResult Detail(long id)
        {
            var model = new OrderDao().ViewDetail(id);
            ViewBag.ListStatus = new StatusDao().GetListStatus();
            return View(model);        
        }

        
        public JsonResult Edit(int StatusId, int ID)
        {
            if (ModelState.IsValid)
            {
                var dao = new OrderDao();
                var model = new Order();
                model.ID = ID;
                model.StatusId = StatusId;
                var result = dao.Update(model);
                if (result)
                {
                    SetAlert("Cập nhật đơn hàng thành công", "success");
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetData(string statusId = null, string fromDate = null, 
            string toDate = null, int page = 1, int pageSize = 10, string searchString = null)
        {
            var list = new List<OrderStatusView>();
            int totalRecord = 0;
            if (fromDate != null && toDate != null)
            {
                list = new OrderDao().GetOrderByDate(Convert.ToInt32(statusId), 
                    fromDate.Replace("/", "-"), toDate.Replace("/", "-"), page, pageSize, ref totalRecord, searchString);
            }

            ViewBag.TotalRecord = (totalRecord > 0 ? totalRecord / pageSize : 0);
            return PartialView("_ListOrderByDate", list);
        }

        public void ExportToExcel()
        {
            var listOrder = new OrderDao().GetAllOrderToExcel();
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");

            ws.Cells["A1"].Value = "Category";
            ws.Cells["B1"].Value = "Order";

            ws.Cells["A2"].Value = "Report";
            ws.Cells["B2"].Value = "Total Order Monthly";

            ws.Cells["A3"].Value = "Date";
            ws.Cells["B3"].Value = string.Format("{0:dd MMMM yyyy} at {0:H: mm tt}", DateTimeOffset.Now);

            ws.Cells["A6"].Value = "OrderCode";
            ws.Cells["B6"].Value = "Created Date";
            ws.Cells["C6"].Value = "ShipName";
            ws.Cells["D6"].Value = "ShipMobile";
            ws.Cells["E6"].Value = "ShipEmail";
            ws.Cells["F6"].Value = "ShipAddress";
            ws.Cells["G6"].Value = "StatusName";

            int rowStart = 7;
            foreach (var item in listOrder)
            {
                if (item.StatusId != 3)
                {
                    ws.Row(rowStart).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Row(rowStart).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("pink")));

                }

                ws.Cells[string.Format("A{0}", rowStart)].Value = item.OrderCode;
                ws.Cells[string.Format("B{0}", rowStart)].Value = item.CreatedDate.ToString();
                ws.Cells[string.Format("C{0}", rowStart)].Value = item.ShipName;
                ws.Cells[string.Format("D{0}", rowStart)].Value = item.ShipMobile;
                ws.Cells[string.Format("E{0}", rowStart)].Value = item.ShipEmail;
                ws.Cells[string.Format("F{0}", rowStart)].Value = item.ShipAddress;
                ws.Cells[string.Format("G{0}", rowStart)].Value = item.StatusName;
                rowStart++;
            }

            ws.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "ExcelReport.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}