﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class ProductCategoryDao
    {
        OnlineShopDbContext db = null;
        public ProductCategoryDao()
        {
            db = new OnlineShopDbContext();
        }

        public List<ProductCategory> ListAll()
        {
            return db.ProductCategories.Where(x => x.Status == true).OrderBy(x => x.DisplayOrder).ToList();
        }

        public ProductCategory ViewDetail(long id)
        {
            return db.ProductCategories.Find(id);
        }

        public string GetCateName(int cateId)
        {
            var item = db.ProductCategories.Where(x => x.ID == cateId).FirstOrDefault();
            string result = null;
            if (item != null) result = item.Name;
            return result;
        }

        public List<ProductCategory> GetListCategory()
        {
            return db.ProductCategories.Where(x => x.Status == true).ToList();
        }

        public ProductCategory GetChildCategoryByParentId(int parentId)
        {
            return db.ProductCategories.Where(x => x.ParentID == parentId).FirstOrDefault();
        }

        public long Insert(ProductCategory productCategory)
        {
            db.ProductCategories.Add(productCategory);
            db.SaveChanges();
            return productCategory.ID;
        }

    }
}
