﻿using Model.EF;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.Dao.Constants;

namespace Model.Dao
{
    public class StatusDao
    {
        OnlineShopDbContext db = null;

        public StatusDao()
        {
            db = new OnlineShopDbContext();
        }
        public string GetStatusName(int statusId)
        {
            var item = db.Statuss.Where(x => x.StatusId == statusId).FirstOrDefault();
            string result = null;
            if (item != null) result = item.StatusName;
            return result;
        }        

        public List<Status> GetListStatus()
        {
            return db.Statuss.Where(x => x.IsDeleted == false).ToList();
        }        
    }

    
}
