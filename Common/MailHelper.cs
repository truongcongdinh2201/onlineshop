﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;

namespace Common
{
    public  class MailHelper
    {
        public void SendMail(string toEmailAddress, string subject,string content)
        {
            var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var fromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
            var fromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            var smtpHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var smtpPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();

            bool enabledSsl = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());
            
            string body = content;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), 
                new MailAddress(toEmailAddress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = content;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
            client.Host = smtpHost;
            client.EnableSsl = enabledSsl;
            client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
            client.Send(message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="detailData"></param>
        /// <returns></returns>
        public static string ReplaceDetailEmailToken(string str, List<object> detailData)
        {
            string strDetail = "";
            string pattern = "{{(.*?)}}";
            var detailMatch = Regex.Match(str, pattern, RegexOptions.Singleline);

            if (detailMatch.Success)
            {
                pattern = "(?<={\\[).*?(?=\\]})";
                foreach (object row in detailData)
                {
                    var detailRow = detailMatch.Value.Replace("{{", "").Replace("}}", "");
                    var matches = Regex.Matches(detailRow, pattern);

                    foreach (Match match in matches)
                    {
                        var value = GetPropertyValue<object>(row, match.Value);
                        if (value != null)
                        {
                            if (value.GetType() == typeof(DateTime))
                            {
                                detailRow = detailRow.Replace("{[" + match.Value + "]}", string.Format("{0:dd/MM/yyyy}", value));
                            }
                            else
                            {
                                detailRow = detailRow.Replace("{[" + match.Value + "]}", value.ToString());
                            }
                        }
                        else
                        {
                            detailRow = detailRow.Replace("{[" + match.Value + "]}", string.Empty);
                        }
                    }
                    strDetail += detailRow;
                }
                return str.Replace(detailMatch.Value, strDetail);
            }
            return str;
        }

        public static string ReplaceEmailToken(string str, object objData)
        {
            string pattern = "(?<=\\[\\[).*?(?=\\]\\])";
            MatchCollection matches = Regex.Matches(str, pattern);

            foreach (Match match in matches)
            {
                var value = GetPropertyValue<object>(objData, match.Value);
                if (value != null)
                {
                    if (value.GetType() == typeof(DateTime))
                    {
                        str = str.Replace("[[" + match.Value + "]]", string.Format("{0:dd/MM/yyyy}", value));
                    }
                    else
                    {
                        str = str.Replace("[[" + match.Value + "]]", value.ToString());
                    }
                }
                else
                {
                    str = str.Replace("[[" + match.Value + "]]", string.Empty);
                }
            }
            return str;
        }
        /// <summary>
        /// Get property of an object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objData"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static T GetPropertyValue<T>(object objData, string propertyName)
        {
            try
            {
                var pro = objData.GetType().GetProperty(propertyName);
                if (pro != null)
                    return (T)pro.GetValue(objData, null);
                return default(T);
            }
            catch
            {
                return default(T);
            }
        }
    }
}
