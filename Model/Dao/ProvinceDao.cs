﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class ProvinceDao
    {
        OnlineShopDbContext db = null;
        public ProvinceDao()
        {
            db = new OnlineShopDbContext();
        }
        public List<Province> GetAllListProvince()
        {
            var list = new List<Province>();
            try
            {
                list = db.Provinces.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public long Insert(Province province)
        {
            try
            {
                db.Provinces.Add(province);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
            return province.ID;
        }

        public bool InsertAllProvince(List<Province> listProvince)
        {
            bool result = false;
            try
            {
                db.Provinces.AddRange(listProvince);
                db.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public string GetProvinceName(int? provinceId)
        {
            var item = db.Provinces.Where(x => x.ID == provinceId).FirstOrDefault();
            string result = null;
            if (item != null) result = item.ProvinceName;
            return result;
        }
    }
}
