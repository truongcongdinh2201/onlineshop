﻿namespace Model.EF
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("Province")]
    public class Province
    {
        [Key]
        public int ID { get; set; }

        [MaxLength(500)]
        public string ProvinceName { get; set; }

    }
}
