﻿var user = {
    init: function () {

        user.loadProvince();                
        user.registerEvent();
    },
    registerEvent: function () {
        $('#ddlProvince').off('change').on('change', function () {
            var id = $(this).val();
            if (id != '') {
                user.loadDistrict(parseInt(id));
            }
            else {
                $('#ddlDistrict').html('');
            }
        });

        $('#ddlDistrict').off('change').on('change', function () {
            var id = $(this).val();
            var provinceID = $('#ddlProvince').val();
            if (id != '') {
                user.loadWard(parseInt(id), parseInt(provinceID));
            }
            else {
                $('#ddlWard').html('');
            }           
        });
    },
    loadProvince: function () {
        $.ajax({
            url: '/User/LoadProvince',
            type: "GET",
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    var html = '<option value="">--Chọn tỉnh thành--</option>';
                    var data = response.data;
                    console.log(data);
                    $.each(data, function (i, item) {
                        html += '<option value="' + item.ID + '">' + item.ProvinceName + '</option>'                        
                    });
                    $('#ddlProvince').html(html);
                }
            }
        })
    },
    loadDistrict: function (id) {
        $.ajax({
            url: '/User/LoadDistrict',            
            data: { provinceId: id },
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    var html = '<option value="">--Chọn quận huyện--</option>';
                    var data = response.data;
                    console.log(data);
                    $.each(data, function (i, item) {
                        html += '<option value="' + item.ID + '">' + item.DistrictName + '</option>'
                    });
                    $('#ddlDistrict').html(html);
                }
            }
        })
    },
    loadWard: function (id) {
        $.ajax({
            url: '/User/LoadWard',            
            data: { districtId: id},
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    var html = '<option value="">--Chọn phường/xã--</option>';
                    var data = response.data;
                    console.log(data);
                    $.each(data, function (i, item) {
                        html += '<option value="' + item.ID + '">' + item.WardName + '</option>'
                    });
                    $('#ddlWard').html(html);
                }
            }
        })
    }
}
user.init();
