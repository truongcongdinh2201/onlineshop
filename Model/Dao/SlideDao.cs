﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.EF;
using System.Threading.Tasks;
using PagedList;

namespace Model.Dao
{
   public  class SlideDao
    {
        OnlineShopDbContext db = null;
        public SlideDao()
        {
            db = new OnlineShopDbContext();
        }
        //Slide for Client
        public List<Slide> ListAllSlide()
        {
            return db.Slides.Where(x => x.Status == true).OrderBy(y => y.DisplayOrder).ToList();
        }

        public IEnumerable<Slide> ListAllPagingSlide(string searchString, int page, int pageSize)
        {
            IQueryable<Slide> model = db.Slides;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.ImageName.Contains(searchString));
            }
            return model.OrderByDescending(x => x.CreatedDate).ToPagedList(page, pageSize);
        }
        //Get ID
        public Slide GetSlideById(int id)
        {
            var item = db.Slides.Find(id);            
            return item;
        }
        // Insert Slide For Admin
        public long Insert(Slide slide)
        {
            db.Slides.Add(slide);
            db.SaveChanges();
            return slide.ID;
        }
        // Update Product For Admin
        public bool Update(Slide item)
        {
            bool rs = false;
            try
            {
                if (item == null)
                    return false;

                var obj = ToEntity(item);
                db.SaveChanges();
                rs = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rs;
        }
        // Update Slide For Admin
        internal Slide ToEntity(Slide dto)
        {
            if (dto == null) return null;
            Slide entity = null;
            if (dto.ID != 0)
            {
                entity = db.Slides.SingleOrDefault(x => x.ID == dto.ID);
                if (entity != null)
                {
                    if (dto.Image != null)
                    {
                        entity.Image = dto.Image;
                    }                                       
                    entity.DisplayOrder = dto.DisplayOrder;
                    entity.Link = dto.Link;                                                     
                    entity.Status = dto.Status;                                       
                }
            }
            return entity;
        }
        // Delete Product For Admin
    }
}
