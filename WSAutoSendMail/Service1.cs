﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.IO;
using System.Data.SqlClient;
using Model.Dao;
using Model.EF;
namespace WSAutoSendMail
{
    public partial class Service1 : ServiceBase
    {
        System.Threading.Timer timer; // name space(using System.Timers;)
        public Service1()
        {
            InitializeComponent();
        }        

        protected override void OnStart(string[] args)
        {
            Library.WriteErrorLog("OnStart Start timer");
            ScheduleServiceEveryDaily();
        }
        public void Start()
        {
            Library.WriteErrorLog("OnStart Start timer");
            ScheduleServiceEveryDaily();
            DateTime currentDate =DateTime.Now;
            SendMailAuto(currentDate.ToString("yyyy-MM-dd"));
        }

        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
            timer.Dispose();
        }

        private string createEmailBody(string userName, string title, string message)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "//HTML//template.html"))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{UserName}", userName); //replacing the required things  

            body = body.Replace("{Title}", title);

            body = body.Replace("{message}", message);

            return body;

        }

        public void SendMailAuto(string currentDate)
        {            
            var listMail = new MailDao().ListMail();
            var listNewProduct = new ProductDao().GetNewProduct(currentDate);
            if (listNewProduct != null && listNewProduct.Count > 0 )
            {
                if (listMail != null && listMail.Count > 0)
                {
                    foreach (var item in listMail)
                    {
                        SendMail(item.Email);
                    }
                }
            }
            
        }
        public void SendMail(string emailTo)
        {
            string email = ConfigurationManager.AppSettings["Email"];
            string password = ConfigurationManager.AppSettings["Password"];
            string host = ConfigurationManager.AppSettings["Host"];
            string port = ConfigurationManager.AppSettings["Port"];
            // cấu hình thông tin mail

            var smtp = new SmtpClient
            {
                Host = host,
                Port = Convert.ToInt32(port),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(email, password)

            };            
            var htmlBody = createEmailBody("", "Test", "Test ABC");
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new System.Net.Mail.MailAddress(email);
            mailMessage.To.Add(emailTo);
            mailMessage.Subject = "Men Fashion Shop";
            ContentType mimeType = new System.Net.Mime.ContentType("text/html");
            AlternateView alternate = AlternateView.CreateAlternateViewFromString(htmlBody, mimeType);
            mailMessage.AlternateViews.Add(alternate);

            mailMessage.Body = htmlBody;

            {
                smtp.Send(mailMessage);
            }
        }
        public void ScheduleService(string mode)
        {
            try
            {
                timer = new System.Threading.Timer(new TimerCallback(SchedularCallback));

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;

                if (mode == "DAILY")
                {
                    //Get the Scheduled Time from AppSettings.
                    scheduledTime = DateTime.Parse(ConfigurationManager.AppSettings["ScheduledTime"]);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next day.
                        scheduledTime = scheduledTime.AddDays(1);
                        
                    }
                }
                if (mode == "INTERVAL")
                {
                    //Get the Interval in Minutes from AppSettings.
                    int intervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);

                    //Set the Scheduled Time by adding the Interval to Current Time.
                    scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next Interval.
                        scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                    }
                }
                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                Library.WriteErrorLog("Simple Service scheduled to run after: " + schedule);
                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                timer.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {

                Library.WriteErrorLog("Simple Service Error on: {0} " + ex.Message + ex.StackTrace);
                ////Stop the Windows Service.
                //using (ServiceController serviceController = new ServiceController("NowDelivery-WS"))
                //{
                //    serviceController.Stop();
                //}
            }
        }

        public void ScheduleServiceEveryDaily()
        {
            try
            {
                string mode = ConfigurationManager.AppSettings["Mode"];
                ScheduleService(mode);
                DateTime currentDate = DateTime.Now;
                SendMailAuto(currentDate.ToString("yyyy-MM-dd"));
                //ReadFileExcel();
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex.ToString());
            }
        }

        private void SchedularCallback(object e)
        {
            Library.WriteErrorLog("CallBack ScheduleServiceEveryDaily Start timer");
            ScheduleServiceEveryDaily();
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
