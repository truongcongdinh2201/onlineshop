﻿using Model.EF;
using Model.ViewModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.Dao.Constants;

namespace Model.Dao
{
    public class ProductDao
    {
        OnlineShopDbContext db = null;
        public ProductDao()
        {
            db = new OnlineShopDbContext();
        }
        // Get New Product
        public List<Product> ListNewProduct(int top)
        {
            return db.Products.OrderByDescending(x => x.CreatedDate).Take(top).ToList();
        }
        public List<string> ListName(string keyword)
        {
            return db.Products.Where(x => x.Name.Contains(keyword)).Select(x => x.Name).ToList();
        }

        /// <summary>
        /// Get list product by category
        /// </summary>
        /// <param name="categoryID"></param>
        /// <returns></returns>
        // Get List Product By Category For Client
        public List<ProductViewModel> ListByCategoryId(long categoryID, ref int totalRecord, int pageIndex = 1, int pageSize = 2)
        {
            totalRecord = db.Products.Where(x => x.CategoryID == categoryID).Count();
            var model = (from a in db.Products
                         join b in db.ProductCategories
                         on a.CategoryID equals b.ID
                         where a.CategoryID == categoryID
                         select new
                         {
                             CateMetaTitle = b.MetaTitle,
                             CateName = b.Name,
                             CreatedDate = a.CreatedDate,
                             ID = a.ID,
                             Images = a.Image,
                             Name = a.Name,
                             MetaTitle = a.MetaTitle,
                             Price = a.Price
                         }).AsEnumerable().Select(x => new ProductViewModel()
                         {
                             CateMetaTitle = x.MetaTitle,
                             CateName = x.Name,
                             CreatedDate = x.CreatedDate,
                             ID = x.ID,
                             Images = x.Images,
                             Name = x.Name,
                             MetaTitle = x.MetaTitle,
                             Price = x.Price,
                             CategoryId = (int)categoryID
                         });
            model.OrderByDescending(x => x.CreatedDate).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return model.ToList();
        }
        // Search Product
        public List<ProductViewModel> Search(string keyword, ref int totalRecord, int pageIndex = 1, int pageSize = 10)
        {
            totalRecord = db.Products.Where(x => x.Name == keyword).Count();
            var model = (from a in db.Products
                         join b in db.ProductCategories
                         on a.CategoryID equals b.ID
                         where a.Name.Contains(keyword) || b.Name.Contains(keyword) || a.Name == keyword
                         select new
                         {
                             CateMetaTitle = b.MetaTitle,
                             CateName = b.Name,
                             CreatedDate = a.CreatedDate,
                             ID = a.ID,
                             Images = a.Image,
                             Name = a.Name,
                             MetaTitle = a.MetaTitle,
                             Price = a.Price
                         }).AsEnumerable().Select(x => new ProductViewModel()
                         {
                             CateMetaTitle = x.MetaTitle,
                             CateName = x.Name,
                             CreatedDate = x.CreatedDate,
                             ID = x.ID,
                             Images = x.Images,
                             Name = x.Name,
                             MetaTitle = x.MetaTitle,
                             Price = x.Price
                         });
            model.OrderByDescending(x => x.CreatedDate).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return model.ToList();
        }
        /// <summary>
        /// List feature product
        /// </summary>
        /// <param name="top"></param>
        /// <returns></returns>
        // List Hot Product
        public List<Product> ListFeatureProduct(int top)
        {
            return db.Products.Where(x => x.TopHot != null && x.TopHot > DateTime.Now).OrderByDescending(x => x.CreatedDate).Take(top).ToList();
        }
        // List Related Product
        public List<Product> ListRelatedProducts(long productId, int top)
        {
            var product = db.Products.Find(productId);
            return db.Products.Where(x => x.ID != productId && x.CategoryID == product.CategoryID).Take(top).ToList();
        }
        // View Detail Product
        public Product ViewDetail(long id)
        {
            var item = db.Products.Find(id);
            #region Cach 1
            //var item = db.Products.Find(id);
            //// lay thong tin size
            //if (item != null)
            //{
            //    //gắn size vào sản phẩm
            //    var svProductSize = new ProductSizeDao();
            //    item.ListProductSize = svProductSize.GetListProductSize((int)id);

            //    var svProductColor = new ProductColorDao();
            //    item.ListProductColor = svProductColor.GetListProductColor((int)id);
            //}
            #endregion
            #region Cach 2
            if (item != null)
            {
                //var svProductColor = new ProductColorDao();
                //item.ListProductColor = svProductColor.GetListProductColor((int)id);

                var svProductSize = new ProductSizeDao();
                item.ListProductSize = svProductSize.GetListProductSize((int)id);                
            }            
            #endregion
            return item;            
        }        
        // Get list product for client
        public List<Product> GetAllListProduct(int? page)
        {            
            List<Product> list = db.Products.Where(x => x.Status == true).ToList();
            
            return list.OrderByDescending(x=>x.ID).ToList();
        }
        //Get list product for Admin
        public List<ProductViewForAdmin> ListAllProductPagingForAdmin(string searchString,int? page)
        {
            List<ProductViewForAdmin> model = GetListCateName();
            //IQueryable<Product> model = db.Products;

            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Name.Contains(searchString) || x.CategoryName.Contains(searchString) || x.Name == searchString).ToList();
            }                   

            return model.OrderByDescending(x => x.CreatedDate).ToList();
        }
        // Add new Product for Admin
        public long Insert(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
            return product.ID;
        }
        // Get Product By ID for Admin
        public Product GetProductById(int id)
        {
            var item = db.Products.Find(id);
            if (item!=null)
            {
                var svProductSize = new ProductSizeDao();
                item.ListProductSizeAdmin = svProductSize.GetListProductSizeForAdmin((int)id);
            }
            return item;
        }
        // Update Product For Admin
        public bool Update(Product item)
        {
            bool rs = false;
            try
            {
                if (item == null)
                    return false;

                var obj = ToEntity(item);
                db.SaveChanges();
                rs = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rs;
        }
        // Update Product For Admin
        internal Product ToEntity(Product dto)
        {
            if (dto == null) return null;
            Product entity = null;
            if (dto.ID != 0)
            {
                entity = db.Products.SingleOrDefault(x => x.ID == dto.ID);
                if (entity != null)
                {
                    entity.Name = dto.Name;
                    entity.Code = dto.Code;
                    entity.MetaTitle = dto.MetaTitle;
                    entity.Description = dto.Description;
                    if (dto.Image != null)
                    {
                        entity.Image = dto.Image;
                    }                                     
                    entity.Price = dto.Price;
                    entity.PromotionPrice = dto.PromotionPrice;
                    entity.IncludedVAT = dto.IncludedVAT;
                    entity.Quantity = dto.Quantity;
                    if (dto.CategoryID != null)
                    {
                        entity.CategoryID = dto.CategoryID;
                    }
                    //entity.CategoryID = dto.CategoryID;                    
                    entity.Detail = dto.Detail;                    
                    entity.Status = dto.Status;                                      
                }
            }
            return entity;
        }
        // Delete Product For Admin
        public bool Delete(int id)
        {
            try
            {
                var product = db.Products.Find(id);
                db.Products.Remove(product);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<ProductViewForAdmin> GetListAllProduct()
        {
            var listDB = new List<ProductViewForAdmin>();            
            try
            {
                string strSql = "EXEC " + StoreProcedures.GetListProductForAdmin;
                listDB = db.Database.SqlQuery<ProductViewForAdmin>(strSql).ToList();                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDB;
        }
        // Get CategoryName For Admin 
        public List<ProductViewForAdmin> GetListCateName()
        {
            var listDB = new List<ProductViewForAdmin>();

            //var listResult = new List<Order>();
            try
            {
                string strSql = "EXEC " + StoreProcedures.GetListProductForAdmin;
                listDB = db.Database.SqlQuery<ProductViewForAdmin>(strSql).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDB;
        }

        //public List<Product> GetProductByRangePrice1()
        //{
        //    var listDB = new List<Product>();
        //    try
        //    {
        //        string strSql = "EXEC " + StoreProcedures.GetProductByRangePrice_1;
        //        listDB = db.Database.SqlQuery<Product>(strSql).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return listDB;
        //}

        //public List<Product> GetProductByRangePrice2()
        //{
        //    var listDB = new List<Product>();
        //    try
        //    {
        //        string strSql = "EXEC " + StoreProcedures.GetProductByRangePrice_2;
        //        listDB = db.Database.SqlQuery<Product>(strSql).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return listDB;
        //}

        //public List<Product> GetProductByRangePrice3()
        //{
        //    var listDB = new List<Product>();
        //    try
        //    {
        //        string strSql = "EXEC " + StoreProcedures.GetProductByRangePrice_3;
        //        listDB = db.Database.SqlQuery<Product>(strSql).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return listDB;
        //}
        // Search Range of Price For Client
        //public List<Product> GetProductByRangePrice(string priceFrom, string priceTo, string categoryId, int? page)
        //{
        //    var listResult = new List<Product>();
        //    try
        //    {
        //        var prPriceFrom = new SqlParameter("@pricefrom", priceFrom);
        //        var prPriceTo = new SqlParameter("@priceto", priceTo);
        //        var prCategoryId = new SqlParameter("@categoryId", categoryId);
        //        string strSql = "EXEC " + StoreProcedures.GetProductByRangePrice + " @pricefrom, @priceto,@categoryId";
        //        listResult = db.Database.SqlQuery<Product>(strSql, prPriceFrom, prPriceTo, prCategoryId).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return listResult.ToList();
        //}

        public List<Product> GetProductByRangePrice(string priceFrom, string priceTo, string categoryId, int? page)
        {
            var listResult = new List<Product>();
            try
            {
                var prPriceFrom = new SqlParameter("@pricefrom", priceFrom);
                var prPriceTo = new SqlParameter("@priceto", priceTo);
                var prCategoryId = new SqlParameter("@categoryId", categoryId);
                string strSql = "EXEC " + StoreProcedures.GetProductByRangePrice + " @pricefrom, @priceto,@categoryId";
                listResult = db.Database.SqlQuery<Product>(strSql, prPriceFrom, prPriceTo, prCategoryId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult.ToList();
        }

        public List<Product> GetNewProduct(string currentDate)
        {
            var listResult = new List<Product>();
            try
            {
                DateTime? currentDateDT = DateTime.ParseExact(currentDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                var prCurrentDate = new SqlParameter("@currentDate", currentDateDT.HasValue ? currentDateDT : (object)DBNull.Value);                
                string strSql = "EXEC " + StoreProcedures.GetNewProduct + " @currentDate";
                listResult = db.Database.SqlQuery<Product>(strSql, prCurrentDate).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult.ToList();
        }

        public List<Product> GetProductByRangePriceAndSearchString(string priceFrom, string priceTo, string searchString, string categoryId, int? page)
        {
            var listResult = new List<Product>();
            try
            {
                
                var prPriceFrom = new SqlParameter("@pricefrom", priceFrom) {
                    Value = string.IsNullOrWhiteSpace(priceFrom) ? (object)DBNull.Value : priceFrom
                };
                var prPriceTo = new SqlParameter("@priceto", priceTo)
                {
                    Value = string.IsNullOrWhiteSpace(priceTo) ? (object)DBNull.Value : priceTo
                };
                var prCategoryId = new SqlParameter("@categoryId", categoryId);
                var prSearchString = new SqlParameter("@searchString", searchString);
                string strSql = "EXEC " + StoreProcedures.GetProductByRangePriceAndSearchString + " @pricefrom, @priceto,@searchString,@categoryId";
                listResult = db.Database.SqlQuery<Product>(strSql, prPriceFrom, prPriceTo, prSearchString, prCategoryId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult.ToList();
        }

    }
}
