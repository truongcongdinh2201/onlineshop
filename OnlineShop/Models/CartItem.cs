﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShop.Models
{
    [Serializable]
    public class CartItem
    {
        public Product Product { set; get; }
        public int Quantity { set; get; }
        //[NotMapped]
        //public int ColorId { get; set; }        
        //[NotMapped]
        //public string ColorName { get; set; }
        [NotMapped]
        public int SizeId { get; set; }
        [NotMapped]
        public string SizeName { get; set; }        
        public string ProvinceId { get; set; }        
        public string DistrictId { get; set; }               
        public string WardId { get; set; }        
    }
}