﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.EF;
using System.Threading.Tasks;
using static Model.Dao.Constants;
using Model.ViewModel;

namespace Model.Dao
{
   public  class ChartDao
    {
        OnlineShopDbContext db = null;
        public ChartDao()
        {
            db = new OnlineShopDbContext();
        }

        public List<GetMonth> ChartOrder()
        {
            var listDB = new List<GetMonth>();
            try
            {
                string strSql = "EXEC " + StoreProcedures.ChartOrder;
                listDB = db.Database.SqlQuery<GetMonth>(strSql).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDB;
        }
    }
}
