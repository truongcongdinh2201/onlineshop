﻿using Model.EF;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.Dao.Constants;

namespace Model.Dao
{
    public class OrderDetailDao
    {
        OnlineShopDbContext db = null;
        public OrderDetailDao()
        {
            db = new OnlineShopDbContext();
        }
        public bool Insert(OrderDetail detail)
        {
            try
            {
                db.OrderDetails.Add(detail);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;

            }
        }

        public List<OrderDetail> GetOrderDetailForAdmin(int orderId)
        {
            var listDB = new List<OrderDetailView>();

            var listResult = new List<OrderDetail>();
            try
            {
                var prOrderId = new SqlParameter("@orderId", orderId);
                string strSql = "EXEC " + StoreProcedures.GetForAdmin + " @orderId";
                listDB = db.Database.SqlQuery<OrderDetailView>(strSql, prOrderId).ToList();
                // >0
                if (listDB != null && listDB.Count > 0)
                {
                    foreach (var detail in listDB)
                    {
                        var item = new OrderDetail();
                        item.OrderID = detail.OrderID;
                        item.ProductID = detail.ProductID;
                        item.Name = detail.Name;
                        item.SizeId = detail.SizeId;
                        item.SizeName = detail.SizeName;
                        item.ColorId = detail.ColorId;
                        item.ColorName = detail.ColorName;
                        item.Price = detail.Price;
                        item.Quantity = detail.Quantity;
                        listResult.Add(item);
                    }
                }
                //
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult;
        }
    }
}
