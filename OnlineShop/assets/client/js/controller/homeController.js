﻿var emailRegister = {
    init: function () {
        emailRegister.registerEvent();
    },
    registerEvent: function () {        
        $('#btnRegister').off('click').on('click', function () {
            var email = $('#txtEmail').val();            

            $.ajax({
                url: '/Home/GetMailFromClient',
                type: 'POST',
                dataType: 'json',
                data: {email: email},
                success: function (response) {                    
                    if (response.status == true) {
                        window.alert('Chúc mừng bạn đã đăng ký thành công!');
                        emailRegister.resetForm();
                    }
                }
            });
        });
    },
    resetForm: function () {
        $('#txtEmail').val('');        
    }
}
emailRegister.init();