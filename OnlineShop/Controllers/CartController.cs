﻿using Model.Dao;
using OnlineShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Model.EF;
using Common;
using System.Configuration;
using System.IO;
using System.Xml.Linq;

namespace OnlineShop.Controllers
{
    public class CartController : Controller
    {
        private const string CartSession = "CartSession";
        // GET: Cart
        public ActionResult Index()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return View(list);
        }

        public JsonResult DeleteAll()
        {
            Session[CartSession] = null;
            return Json(new
            {
                status = true
            });
        }

        public JsonResult Delete(long id)
        {
            var sessionCart = (List<CartItem>)Session[CartSession];
            sessionCart.RemoveAll(x => x.Product.ID == id);
            Session[CartSession] = sessionCart;
            return Json(new
            {
                status = true
            });
        }
        public JsonResult Update(string cartModel)
        {
            var jsonCart = new JavaScriptSerializer().Deserialize<List<CartItem>>(cartModel);
            var sessionCart = (List<CartItem>)Session[CartSession];

            foreach (var item in sessionCart)
            {
                var jsonItem = jsonCart.SingleOrDefault(x => x.Product.ID == item.Product.ID);
                if (jsonItem != null)
                {
                    item.Quantity = jsonItem.Quantity;
                }
            }
            Session[CartSession] = sessionCart;
            return Json(new
            {
                status = true
            });
        }
        public ActionResult AddItem(long productId, int quantity, int productSizeId)
        {
            var product = new ProductDao().ViewDetail(productId);
            var cart = Session[CartSession];    
            int sizeId = new ProductSizeDao().GetSizeId(productSizeId);
            if (cart != null)
            {
                var list = (List<CartItem>)cart;  
                if (list.Exists(x => x.Product.ID == productId  && x.SizeId == sizeId))   
                {
                    foreach (var item in list)
                    {
                        if (item.Product.ID == productId  && item.SizeId == sizeId)
                        {                          
                            item.Quantity += quantity;                            
                        }
                    }
                }
                else
                {
                    // create new model
                    var item = new CartItem();
                    item.Product = product;
                    item.Quantity = quantity;                    
                    item.SizeId = sizeId;
                    item.SizeName = new SizeDao().GetSizeName(item.SizeId);
                    list.Add(item);
                }
                //Assign into session
                Session[CartSession] = list;
            }
            else   // no item in cart
            {
                // create new model
                var item = new CartItem();
                item.Product = product;
                item.Quantity = quantity;                
                item.SizeId = sizeId;
                item.SizeName = new SizeDao().GetSizeName(item.SizeId);
                var list = new List<CartItem>();
                list.Add(item);
                //Assign into session
                Session[CartSession] = list;
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Payment()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return View(list);
        }

        //[HttpPost]
        //public ActionResult Payment(CartItem model, string shipName, string mobile, string address, string email, string orderCode)
        //{
        //    var order = new Order();
        //    order.CreatedDate = DateTime.Now;
        //    order.ShipAddress = address;
        //    if (model.ProvinceID != null)
        //    {
        //        order.ProvinceID = int.Parse(model.ProvinceID);
        //    }
        //    if (model.DistrictID != null)
        //    {
        //        order.DistrictID = int.Parse(model.DistrictID);
        //    }
        //    order.ShipMobile = mobile;
        //    order.ShipName = shipName;
        //    order.ShipEmail = email;
        //    order.OrderCode = new OrderDao().GetOrderCode();
        //    order.StatusId = 1;
        //    try
        //    {
        //        var id = new OrderDao().Insert(order);
        //        //var cart = (List<CartItem>)Session[CartSession];
        //        var cart = Session[CartSession];
        //        var list = new List<CartItem>();
        //        if (cart != null)
        //        {
        //            list = (List<CartItem>)cart;
        //        }
        //        var detailDao = new Model.Dao.OrderDetailDao();
        //        decimal total = 0;
        //        var productname = "";
        //        var quantity = 0;
        //        foreach (var item in list)
        //        {
        //            var orderDetail = new OrderDetail();
        //            orderDetail.ProductID = item.Product.ID;
        //            orderDetail.OrderID = id;
        //            orderDetail.SizeId = item.SizeId;
        //            orderDetail.Price = item.Product.Price;
        //            orderDetail.Quantity = item.Quantity;
        //            detailDao.Insert(orderDetail);

        //            productname = item.Product.Name;
        //            quantity = item.Quantity;
        //            total += (item.Product.Price.GetValueOrDefault(0) * item.Quantity);
        //        }
        //        // Real File HTML to Append Data 
        //        string content = System.IO.File.ReadAllText(Server.MapPath("~/assets/client/template/neworder.html"));

        //        content = content.Replace("{{ProductName}}", productname);
        //        content = content.Replace("{{Quantity}}", quantity.ToString("N0"));
        //        content = content.Replace("{{Total}}", total.ToString("N0"));

        //        content = content.Replace("{{CustomerName}}", shipName);
        //        content = content.Replace("{{Phone}}", mobile);
        //        content = content.Replace("{{Email}}", email);
        //        content = content.Replace("{{Address}}", address);

        //        var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

        //        new MailHelper().SendMail(email, "Đơn hàng mới từ OnlineShop", content);  // gui den khach hang
        //        new MailHelper().SendMail(toEmail, "Đơn hàng mới từ OnlineShop", content); // gửi đến quản lí
        //    }
        //    catch (Exception ex)
        //    {
        //        //ghi log
        //        return Redirect("/loi-thanh-toan");
        //    }
        //    return Redirect("/hoan-thanh");
        //}
        [HttpPost]
        public ActionResult Payment(CartItem model, string shipName, string mobile,
            string address, int? province, string provinceName, int? district, string districtName,
            int? ward, string wardName, string email, string orderCode)
        {
            var order = new Order();
            order.ShipName = shipName;
            order.ShipMobile = mobile;            
            order.ShipAddress = address;
            order.ProvinceId = int.Parse(model.ProvinceId);
            order.DistrictId = int.Parse(model.DistrictId);
            order.WardId = int.Parse(model.WardId);
            order.ShipEmail = email;
            order.OrderCode = new OrderDao().GetOrderCode();
            order.CreatedDate = DateTime.Now;            
            order.StatusId = 1;
            try
            {
                var id = new OrderDao().Insert(order);                
                var cart = Session[CartSession];
                var list = new List<CartItem>();
                if (cart != null)
                {
                    list = (List<CartItem>)cart;
                }
                var detailDao = new Model.Dao.OrderDetailDao();
                decimal total = 0;
                var productname = "";
                var quantity = 0;
               
                foreach (var item in list)
                {
                    var orderDetail = new OrderDetail();
                    orderDetail.ProductID = item.Product.ID;
                    orderDetail.OrderID = id;
                    orderDetail.SizeId = item.SizeId;
                    orderDetail.Price = item.Product.Price;
                    orderDetail.Quantity = item.Quantity;
                    detailDao.Insert(orderDetail);

                    productname = item.Product.Name;
                    quantity = item.Quantity;
                    total += (item.Product.Price.GetValueOrDefault(0) * item.Quantity);                    
                }
                if (order.ProvinceId != null)
                {
                    provinceName = new ProvinceDao().GetProvinceName(order.ProvinceId);
                }
                if (order.DistrictId !=null)
                {
                    districtName = new DistrictDao().GetDistrictName(order.DistrictId);
                }
                if (order.WardId != null)
                {
                    wardName = new WardDao().GetWardName(order.WardId);
                }
                // Send Mail
                SendMailOrder(list, email,shipName,mobile,address,provinceName, districtName, wardName);

            }
            catch (Exception ex)
            {                
                return Redirect("/loi-thanh-toan");
            }
            return Redirect("/hoan-thanh");
        }

        public void SendMailOrder(List<CartItem> listItemCart, string email, 
            string customerName, string phone, string address, string provinceName, 
            string districtName, string wardName)
        {
            // Real File HTML to Append Data 
            string content = System.IO.File.ReadAllText(Server.MapPath("~/assets/client/template/neworder.html"));
            Object obj = new
            {
                CustomerName = customerName,
                Phone = phone,
                Address = address,
                ProvinceName = provinceName,
                DistrictName = districtName,
                WardName = wardName
            };
            List<object> list = new List<object>();
            foreach (var item in listItemCart)
            {
                Object detail = new
                {
                    ProductName = item.Product.Name,
                    Quantity = item.Quantity,
                    Total = (item.Product.Price.GetValueOrDefault(0) * item.Quantity),                    
            };
                list.Add(detail);
            }
            var strContent = MailHelper.ReplaceEmailToken(content, obj);
            strContent = MailHelper.ReplaceDetailEmailToken(strContent, list);
            var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

            new MailHelper().SendMail(email, "Đơn hàng mới từ OnlineShop", strContent);  // Send user
            new MailHelper().SendMail(toEmail, "Đơn hàng mới từ OnlineShop", strContent); // Send manager
        }
        public JsonResult LoadProvince()
        {            
            var list = new ProvinceDao().GetAllListProvince();            
            return Json(new
            {
                data = list,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }   
        public JsonResult LoadDistrict(int provinceId)
        {            
            var list = new DistrictDao().GetDistrictByProvinceId(provinceId);            
            return Json(new
            {
                data = list,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadWard(int districtId)
        {            
            var list = new WardDao().GetWardByDistrictId(districtId);
            return Json(new
            {
                data = list,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Success()
        {
            Session[CartSession] = null;
            return View();
        }

    }
}