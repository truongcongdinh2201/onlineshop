﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
using OnlineShop.Common;
using System.IO;
using System.Configuration;
using PagedList;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class ProductController : BaseController
    {
        // GET: Admin/Product
        public ActionResult Index(string searchString, int? page, string currentFilter)
        {
            var model = new ProductDao().ListAllProductPagingForAdmin(searchString,page);
            if (searchString != null)
            {
                page = 1;
            }
            ViewBag.CurrentFilter = searchString;
            // Paging
            int pageSize = 8;
            int pageNumber = (page ?? 1);
            return View(model.ToList().ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            SetViewBagProductCategory();
            return View();
        }

        [HttpPost]        
        public ActionResult Create(Product product)
        {
            if (ModelState.IsValid)
            {
                if (product.ImageFile != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(product.ImageFile.FileName);
                    string extension = Path.GetExtension(product.ImageFile.FileName);
                    fileName = DateTime.Now.ToString("yymmddfff") + extension;
                    product.Image = "/assets/client/images/" + fileName;
                    product.ImageNameDb = fileName;
                    string fileNameForImageName = Path.GetFileNameWithoutExtension(product.ImageFile.FileName);
                    product.ImageName = fileName + extension;
                    product.ImageName = fileNameForImageName;
                    fileName = Path.Combine(Server.MapPath("/assets/client/images/"), fileName);
                    product.ImageFile.SaveAs(fileName);
                    var dao = new ProductDao();
                    product.CreatedDate = DateTime.Now;
                    long id = dao.Insert(product);
                    if (id > 0)
                    {
                        SetAlert("Thêm sản phẩm thành công", "success");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Thêm product không thành công");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Yêu cầu chọn hình ảnh");
                }

            }
            SetViewBagProductCategory(product.CategoryID);
            return View(product);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var product = new ProductDao().GetProductById(id);
            SetViewBagProductCategory(product.CategoryID);
            //SetViewBagSize();
            ViewBag.ListSize = new SizeDao().ListSize();
            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product) 
        {
            if (ModelState.IsValid)
            {
                var dao = new ProductDao();                
                var result = dao.Update(product);
                if (result)
                {
                    SetAlert("Update sản phẩm thành công", "success");                    
                }
                else
                {
                    ModelState.AddModelError("", "Update sản phẩm không thành công");
                }
            }
            SetViewBagProductCategory(product.CategoryID);
            return View(product);
        }
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new ProductDao().Delete(id);

            return RedirectToAction("Index");
        }

        public void SetViewBagProductCategory(long? selectedId = null)
        {
            var dao = new ProductCategoryDao();
            ViewBag.CategoryID = new SelectList(dao.ListAll(), "ID", "Name", selectedId);            
        }

        public void SetViewBagSize(long? selectdSizeId = null)
        {
            var list = new SizeDao();
            ViewBag.ListSize = new SelectList(list.ListSize(), "SizeId", "SizeName", selectdSizeId);
        }

        public JsonResult GetChildProductCategory(int parentId)
        {
            var childProductCategory = new ProductCategoryDao().GetChildCategoryByParentId(parentId);

            return Json(childProductCategory, JsonRequestBehavior.AllowGet);
        }
    }
}