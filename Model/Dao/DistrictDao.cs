﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class DistrictDao
    {
        OnlineShopDbContext db = null;
        public DistrictDao()
        {
            db = new OnlineShopDbContext();
        }
        public List<District> GetAllListDistrict()
        {
            var list = db.Districts.ToList();
            return list;
        }

        public List<District> GetDistrictByProvinceId(int provinceId)
        {
            var list = new List<District>();
            try
            {
                list = db.Districts.Where(x => x.ProvinceId == provinceId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return list;
        }

        public long Insert(District district)
        {
            try
            {
                db.Districts.Add(district);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return district.ID;
        }
        public bool InsertAllDistict(List<District> listDistrict)
        {
            bool result = false;
            try
            {
                db.Districts.AddRange(listDistrict);
                db.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }

        public string GetDistrictName(int? distrcitId)
        {
            var item = db.Districts.Where(x => x.ID == distrcitId).FirstOrDefault();
            string result = null;
            if (item != null) result = item.DistrictName;
            return result;
        }
    }
}
