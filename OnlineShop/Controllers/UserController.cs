﻿using BotDetect.Web.UI.Mvc;
using Facebook;
using Model.Dao;
using Model.EF;
using OnlineShop.Common;
using OnlineShop.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace OnlineShop.Controllers
{
    
    public class UserController : BaseController
    {
        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallback");
                return uriBuilder.Uri;
            }
        }

        // GET: User
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login()
        {

            return View();
        }
        public ActionResult LoginFacebook()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FbAppId"],
                client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email",
            });

            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult FacebookCallback(string code)
        {
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = ConfigurationManager.AppSettings["FbAppId"],
                client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });


            var accessToken = result.access_token;
            if (!string.IsNullOrEmpty(accessToken))
            {
                fb.AccessToken = accessToken;
                // Get the user's information, like email, first name, middle name etc
                dynamic me = fb.Get("me?fields=first_name,middle_name,last_name,id,email");
                string email = me.email;
                string userName = me.email;
                string firstname = me.first_name;
                string middlename = me.middle_name;
                string lastname = me.last_name;

                var user = new User();
                user.Email = email;
                user.UserName = email;
                user.Status = true;
                user.Name = firstname + " " + middlename + " " + lastname;
                user.CreatedDate = DateTime.Now;
                user.GroupID = "MEMBER";
                var resultInsert = new UserDao().InsertForFacebook(user);
                if (resultInsert > 0)
                {
                    var userSession = new UserLogin();
                    userSession.UserName = user.UserName;
                    userSession.UserID = user.ID;
                    userSession.UserEmail = user.Email;
                    Session.Add(CommonConstants.USER_SESSION, userSession);
                }                
            }
            return Redirect("/");
        }
        public ActionResult Logout()
        {
            Session[CommonConstants.USER_SESSION] = null;
            return Redirect("/");
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                var result = dao.Login(model.UserName, Encryptor.MD5Hash(model.Password));
                if (result == 1)
                {
                    var user = dao.GetById(model.UserName);
                    var userSession = new UserLogin();
                    userSession.UserName = user.UserName;
                    userSession.UserID = user.ID;
                    userSession.UserEmail = user.Email;
                    Session.Add(CommonConstants.USER_SESSION, userSession);
                    return Redirect("/");
                }
                else if (result == 0)
                {
                    ModelState.AddModelError("", "Tài khoản không tồn tại.");
                }
                else if (result == -1)
                {
                    ModelState.AddModelError("", "Tài khoản đang bị khoá.");
                }
                else if (result == -2)
                {
                    ModelState.AddModelError("", "Mật khẩu không đúng.");
                }
                else
                {
                    ModelState.AddModelError("", "Đăng nhập không đúng.");
                }
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CaptchaValidation("CaptchaCode", "registerCapcha", "Mã xác nhận không đúng!")]        
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                if (dao.CheckUserName(model.UserName))
                {
                    ModelState.AddModelError("", "Tên đăng nhập đã tồn tại");
                }
                else if (dao.CheckEmail(model.Email))
                {
                    ModelState.AddModelError("", "Email đã tồn tại");
                }
                else
                {
                    var user = new User();
                    user.UserName = model.UserName;
                    user.Name = model.Name;
                    user.Password = Encryptor.MD5Hash(model.Password);
                    user.Phone = model.Phone;
                    user.Email = model.Email;
                    user.Address = model.Address;
                    user.CreatedDate = DateTime.Now;
                    user.Status = true;
                    
                    user.ProvinceId = model.ProvinceId;
                    user.DistrictId = model.DistrictId;
                    user.WardId = model.WardId;
                    user.GroupID = "MEMBER";
                    var result = dao.Insert(user);
                    if (result > 0)
                    {
                        ViewBag.Success = "Đăng ký thành công";
                        model = new RegisterModel();
                    }
                    else
                    {
                        ModelState.AddModelError("", "Đăng ký không thành công.");
                    }
                }
            }
            return View(model);
        }        
        public JsonResult LoadProvince()
        {
            
            var list = new ProvinceDao().GetAllListProvince();            
            return Json(new
            {
                data = list,
                status = true
            },JsonRequestBehavior.AllowGet);
        }        
        public JsonResult LoadDistrict(int provinceId)
        {            
            var list = new DistrictDao().GetDistrictByProvinceId(provinceId);            
            return Json(new
            {
                data = list,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult LoadWard(int districtId)
        {
            
            var list = new WardDao().GetWardByDistrictId(districtId);
            
            return Json(new
            {
                data = list,
                status = true
            },JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetDataProvinceToDB()
        //{
        //    var xmlDoc = XDocument.Load(Server.MapPath(@"~/assets/client/data/Provinces_Data.xml"));

        //    var xElements = xmlDoc.Element("Root").Elements("Item").Where(x => x.Attribute("type").Value == "province");
        //    var listProvinceDB = new List<Province>();
        //    Province province = null;
        //    foreach (var item in xElements)
        //    {
        //        province = new Province();
        //        province.ID = int.Parse(item.Attribute("id").Value);
        //        province.ProvinceName = item.Attribute("value").Value;
        //        listProvinceDB.Add(province);                
        //        GetDataDistrictToDB(province.ID);
        //    }
        //    var id = new ProvinceDao().InsertAllProvince(listProvinceDB);
        //    return Json(listProvinceDB, JsonRequestBehavior.AllowGet);
        //}

        //public void GetDataDistrictToDB(int provinceID)
        //{
        //    var xmlDoc = XDocument.Load(Server.MapPath(@"~/assets/client/data/Provinces_Data.xml"));

        //    var xElement = xmlDoc.Element("Root").Elements("Item")
        //        .Single(x => x.Attribute("type").Value == "province" && int.Parse(x.Attribute("id").Value) == provinceID);
        //    var listDistrictDB = new List<District>();
        //    District district = null;
        //    foreach (var item in xElement.Elements("Item").Where(x => x.Attribute("type").Value == "district"))
        //    {
        //        district = new District();
        //        district.ID = int.Parse(item.Attribute("id").Value);
        //        district.DistrictName = item.Attribute("value").Value;
        //        district.ProvinceId = provinceID;
        //        listDistrictDB.Add(district);
        //        GetDataWardToDB(district.ID);
        //    }
        //    var id = new DistrictDao().InsertAllDistict(listDistrictDB);
        //}
        //public void GetDataWardToDB(int districtId)
        //{
        //    var xmlDoc = XDocument.Load(Server.MapPath(@"~/assets/client/data/Provinces_Data.xml"));

        //    var xElement = xmlDoc.Element("Root").Elements("Item").Elements("Item")
        //        .Single(x => x.Attribute("type").Value == "district" && int.Parse(x.Attribute("id").Value) == districtId);            
        //    var listWardDB = new List<Ward>();
        //    Ward ward = null;
        //    foreach (var item in xElement.Elements("Item").Where(x => x.Attribute("type").Value == "precinct"))
        //    {
        //        ward = new Ward();
        //        ward.ID = int.Parse(item.Attribute("id").Value);
        //        ward.WardName = item.Attribute("value").Value;
        //        listWardDB.Add(ward);
        //        ward.DistrictId = districtId;
        //    }
        //    var id = new WardDao().InsertAllWard(listWardDB);
        //}
    }
}