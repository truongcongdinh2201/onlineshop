namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        public long ID { get; set; }

        public DateTime? CreatedDate { get; set; }

        public long? CustomerID { get; set; }
        [StringLength(50)]
        public string ShipName { get; set; }
        [StringLength(50)]
        public string ShipMobile { get; set; }
        [StringLength(250)]
        public string ShipAddress { get; set; }        
        public int? ProvinceId { get; set; }
        
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        [StringLength(50)]
        public string ShipEmail { get; set; }        
        public int StatusId { get; set; }
        public string OrderCode { get; set; }

        [NotMapped]
        public List<OrderDetail> ListOrderDetail { get; set; }
        [NotMapped]
        public string StatusName { get; set; }
        [NotMapped]
        public List<Status> ListStatus { get; set; }
    }
}
