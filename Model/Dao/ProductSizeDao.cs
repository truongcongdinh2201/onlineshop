﻿using Model.EF;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.Dao.Constants;

namespace Model.Dao
{
    public class ProductSizeDao
    {
        OnlineShopDbContext db = null;
        public ProductSizeDao()
        {
            db = new OnlineShopDbContext();
        }        
        public List<ProductSize> GetListProductSize(int productId)
        {
            var listDB = new List<ProductSizeView>();

            var listResult = new List<ProductSize>();
            try
            {
                var prProductId = new SqlParameter("@productId", productId);
                string strSql = "EXEC " + StoreProcedures.GetListSize + " @productId";
                listDB = db.Database.SqlQuery<ProductSizeView>(strSql, prProductId).ToList();
                // >0
                if (listDB != null && listDB.Count > 0)
                {
                    foreach (var detail in listDB)
                    {
                        var item = new ProductSize();
                        item.ProductSizeId = detail.ProductSizeId;
                        item.SizeName = detail.SizeName;
                        listResult.Add(item);
                    }
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult;
        }

        public List<ProductSize> GetListProductSizeForAdmin(int productId)
        {
            var listDB = new List<ProductSizeAdminView>();

            var listResult = new List<ProductSize>();
            try
            {
                var prProductId = new SqlParameter("@productId", productId);
                string strSql = "EXEC " + StoreProcedures.GetListProductSizeForAdmin + " @productId";
                listDB = db.Database.SqlQuery<ProductSizeAdminView>(strSql, prProductId).ToList();
                // >0
                if (listDB != null && listDB.Count > 0)
                {
                    foreach (var detail in listDB)
                    {
                        var item = new ProductSize();
                        item.ProductSizeId = detail.ProductSizeId;
                        item.ProductId = detail.ProductId;
                        item.Name = detail.Name;
                        item.SizeId = detail.SizeId;
                        item.SizeName = detail.SizeName;
                        item.Quantity = detail.Quantity;
                        item.Status = detail.Status;
                        listResult.Add(item);
                    }
                }
                //
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult;
        }

        public int GetSizeId(int productSizeId)
        {
            var item = db.ProductSizes.Where(x => x.ProductSizeId == productSizeId).FirstOrDefault();
            var result = 0;
            if (item != null) result = item.SizeId;

            return result;
        }

        public bool Delete(int id)
        {
            try
            {
                var productSize = db.ProductSizes.Find(id);
                db.ProductSizes.Remove(productSize);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(ProductSize item)
        {
            bool rs = false;
            try
            {
                if (item == null)
                    return false;

                var obj = ToEntity(item);
                db.SaveChanges();
                rs = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rs;
        }

        internal ProductSize ToEntity(ProductSize dto)
        {
            if (dto == null) return null;
            ProductSize entity = null;
            if (dto.ProductSizeId != 0)
            {
                entity = db.ProductSizes.SingleOrDefault(x => x.ProductSizeId == dto.ProductSizeId);
                if (entity != null)
                {
                    entity.SizeId = dto.SizeId;
                    entity.SizeName = dto.SizeName;
                    entity.Quantity = dto.Quantity;
                }
            }
            return entity;
        }

        public long Insert(ProductSize productSize)
        {
            db.ProductSizes.Add(productSize);
            db.SaveChanges();
            return productSize.ProductSizeId;
        }

        public ProductSize GetProductSizeById(int id)
        {
            var item = db.ProductSizes.Find(id);            
            return item;
        }

        
    }
}
