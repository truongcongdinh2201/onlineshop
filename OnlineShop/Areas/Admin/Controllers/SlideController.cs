﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;
using System.IO;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class SlideController : BaseController
    {
        // GET: Admin/Feedback
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var model = new SlideDao().ListAllPagingSlide(searchString, page, pageSize);
            ViewBag.SearchString = searchString;

            return View(model);
        }

        public ActionResult Create()
        {            
            return View();
        }

        [HttpPost]
        public ActionResult Create(Slide slide)
        {
            if (ModelState.IsValid)
            {
                if (slide.ImageFile != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(slide.ImageFile.FileName);
                    string extension = Path.GetExtension(slide.ImageFile.FileName);
                    fileName = DateTime.Now.ToString("yymmddfff") + extension;
                    slide.Image = "/assets/admin/images/" + fileName;
                    slide.ImageNameDb = fileName;
                    string fileNameForImageName = Path.GetFileNameWithoutExtension(slide.ImageFile.FileName);
                    slide.ImageName = fileName + extension;
                    slide.ImageName = fileNameForImageName;
                    fileName = Path.Combine(Server.MapPath("/assets/admin/images/"), fileName);
                    slide.ImageFile.SaveAs(fileName);
                    var dao = new SlideDao();
                    long id = dao.Insert(slide);
                    if (id > 0)
                    {
                        SetAlert("Thêm slide thành công", "success");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Thêm slide không thành công");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Yêu cầu chọn hình ảnh");
                }
            }
            return View(slide);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var slide = new SlideDao().GetSlideById(id);            
            
            return View(slide);
        }

        [HttpPost]
        public ActionResult Edit(Slide slide)
        {
            if (ModelState.IsValid)
            {                
                var dao = new SlideDao();
                var result = dao.Update(slide);
                if (result)
                {
                    SetAlert("Cập nhật slide thành công", "success");
                }
                else
                {
                    ModelState.AddModelError("", "Cập nhật slide không thành công");
                }
            }            
            return View(slide);
        }
    }
}