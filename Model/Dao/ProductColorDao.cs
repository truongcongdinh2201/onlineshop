﻿using Model.EF;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.Dao.Constants;

namespace Model.Dao
{
    public class ProductColorDao
    {
        OnlineShopDbContext db = null;
        public ProductColorDao()
        {
            db = new OnlineShopDbContext();
        }

        //public List<ProductColor> GetListProductColor(int productId)
        //{
        //    var list = db.ProductColors.Where(x => x.ProductId == productId).ToList();
        //    if (list != null && list.Count>0)
        //    {
        //        var sv = new ColorDao();
        //        foreach (var item in list)
        //        {
        //            item.ColorName = sv.GetColorName(item.ColorId);
        //        }
        //    }
        //    return list;
        //    // return db.ProductColors.Where(x => x.ProductId == productId).ToList();
        //}

        public List<ProductColor> GetListProductColor(int productId)
        {
            var listDB = new List<ProductColorView>();

            var listResult=new List<ProductColor>();
            try
            {                
                var prProductId = new SqlParameter("@productId", productId);                                
                string strSql = "EXEC " + StoreProcedures.GetListColor + " @productId";
                listDB = db.Database.SqlQuery<ProductColorView>(strSql, prProductId).ToList();
                // >0
                if (listDB != null && listDB.Count > 0)
                {
                    foreach (var detail in listDB)
                    {
                        var item = new ProductColor();
                        item.ProductColorId = detail.ProductColorId;
                        item.ColorName = detail.ColorName; // lấy thuộc tính ColorName trong bảng Color
                        item.Image = detail.Image;
                        listResult.Add(item);
                    }
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult;
        }
        
        public string GetImageByProductColorId(int productColorId)
        {           
            var result = "";
            try
            {
                var prproductColorId = new SqlParameter("@productColorId", productColorId);
                string strSql = "EXEC " + StoreProcedures.GetColorImage + " @productColorId";
                result = db.Database.SqlQuery<string>(strSql, prproductColorId).FirstOrDefault();               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public int GetColorId(int productColorId)
        {
            var item = db.ProductColors.Where(x => x.ProductColorId == productColorId).FirstOrDefault();
            var result = 0;
            if (item != null) result = item.ColorId;
            
            return result;
        }
    }
}
