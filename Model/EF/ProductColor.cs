﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.EF
{
    [Table("ProductColor")]
    public class ProductColor
    {
        [Key]
        
        public int ProductColorId { get; set; }
        public int ProductId { get; set; }
        public int ColorId { get; set; }
        public bool? Status { get; set; }
        public string Image { get; set; }

        [NotMapped]
        public string ColorName { get; set; }
    }
}
