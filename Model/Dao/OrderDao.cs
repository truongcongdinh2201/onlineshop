﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;
using PagedList;
using Model.ViewModel;
using System.Data.SqlClient;
using static Model.Dao.Constants;
using System.Globalization;

namespace Model.Dao
{
    public class OrderDao
    {
        OnlineShopDbContext db = null;
        public OrderDao()
        {
            db = new OnlineShopDbContext();
        }
        public long Insert(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
            return order.ID;
        }

        //public IEnumerable<Order> ListAllPaging(string searchString, int page, int pageSize)
        //{
        //    //IQueryable<Order> model = db.Orders;
        //    IQueryable<Order> model = GetListOrderStatus().AsQueryable();

        //    if (!string.IsNullOrEmpty(searchString))
        //    {
        //        model = model.Where(x => x.ShipName.Contains(searchString));
        //    }
        //    if (model != null)
        //    {

        //    }
        //    return model.OrderByDescending(x => x.CreatedDate).ToPagedList(page, pageSize);
        //}        

        public IEnumerable<OrderStatusView> ListAllOrderPagingForAdmin(string searchString, int page, int pageSize)
        {
            //IQueryable<Order> model = db.Orders;
            IQueryable<OrderStatusView> model = GetListOrderStatus().AsQueryable();

            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.ShipName.Contains(searchString) || x.ShipMobile.Contains(searchString));
            }            
            return model.OrderByDescending(x => x.CreatedDate).ToPagedList(page, pageSize);
        }

        public Order ViewDetail(long orderId)
        {
            var item = db.Orders.Find(orderId);
            if (item != null)
            {
                var svOrderDetail = new OrderDetailDao();
                item.ListOrderDetail = svOrderDetail.GetOrderDetailForAdmin((int)orderId);

                var svStatus = new StatusDao();
                item.StatusName = svStatus.GetStatusName(item.StatusId);                
            }            
            return item;
        }

        public List<OrderStatusView> GetListOrderStatus()
        {
            var listDB = new List<OrderStatusView>();

            //var listResult = new List<Order>();
            try
            {
                string strSql = "EXEC " + StoreProcedures.GetListStatus;
                listDB = db.Database.SqlQuery<OrderStatusView>(strSql).ToList();                            
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDB;
        }

        internal Order ToEntity(Order dto)
        {
            if (dto == null) return null;
            Order entity = null;
            if (dto.ID != 0)
            {
                entity = db.Orders.SingleOrDefault(x => x.ID == dto.ID);
                if (entity != null)
                {
                    entity.StatusId = dto.StatusId;
                }
            }
            return entity;
        }
        // update order

        public bool Update(Order item)
        {
            bool rs = false;
            try
            {
                if (item == null)
                    return false;

                var obj = ToEntity(item);
                db.SaveChanges();
                rs = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rs;
        }

        public string GetOrderCode()
        {
           string rs = null;
            try
            {
                var prOrderCode = new SqlParameter()
                {
                    ParameterName = "@OrderCode",
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Size = 15,
                    Direction = System.Data.ParameterDirection.Output
                };
                string strSql = "EXEC " + StoreProcedures.GetCodeOrder + " @OrderCode OUTPUT";
                db.Database.ExecuteSqlCommand(strSql, prOrderCode);
                rs = prOrderCode.Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rs;
        }
        public List<OrderStatusView> GetOrderByDate(int statusId, string fromDate, string toDate, 
            int page, int pageSize, ref int totalRecord, string searchString)
        {
            var listResult = new List<OrderStatusView>();
            try
            {
                DateTime? fromDateDT = DateTime.ParseExact(fromDate, "d-M-yyyy", CultureInfo.InvariantCulture);
                DateTime? toDateDT = DateTime.ParseExact(toDate, "d-M-yyyy", CultureInfo.InvariantCulture);
                var prFromDate = new SqlParameter("@fromDate", fromDateDT.HasValue ? fromDateDT : (object)DBNull.Value);
                var prToDate = new SqlParameter("@toDate", toDateDT.HasValue ? toDateDT : (object)DBNull.Value);
                var prStatusId = new SqlParameter("@statusId", statusId);
                var prSearchString = new SqlParameter("@searchString", searchString);
                string strSql = "EXEC " + StoreProcedures.SearchOrderByDate + " @fromDate, @toDate,@statusId,@searchString";
                listResult = db.Database.SqlQuery<OrderStatusView>(strSql, prFromDate, prToDate, prStatusId, prSearchString).ToList();
                totalRecord = listResult.Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listResult.Skip((page - 1) * pageSize).Take(pageSize).ToList();
        }

        public static DateTime? GetNullableDateTime(object obj, bool getTime = false)
        {
            DateTime dtReturn;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                var sValue = obj.ToString().Trim();
                if (DateTime.TryParseExact(sValue, "d-M-yyyy", new CultureInfo("vi-VN"), DateTimeStyles.None, out dtReturn))
                {
                    return dtReturn;
                }
                if (DateTime.TryParseExact(sValue, "d/M/yyyy", new CultureInfo("vi-VN"), DateTimeStyles.None, out dtReturn))
                {
                    return dtReturn;
                }
                // convert double value to datetime
                double dateDouble;
                if (double.TryParse(sValue, out dateDouble))
                    return DateTime.FromOADate(dateDouble);
            }

            return null;
        }        

        public List<OrderStatusView> GetAllOrderToExcel()
        {
            var listDB = new List<OrderStatusView>();

            //var listResult = new List<Order>();
            try
            {
                string strSql = "EXEC " + StoreProcedures.ExportOrderToExcel;
                listDB = db.Database.SqlQuery<OrderStatusView>(strSql).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDB;
        }
    }
}
