﻿using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.Dao.Constants;
namespace Model.Dao
{
    public class MailDao
    {
        OnlineShopDbContext db = null;
        public MailDao()
        {
            db = new OnlineShopDbContext();
        }

        public List<Mail> ListMail()
        {
            var listDB = new List<Mail>();
            try
            {
                string strSql = "EXEC " + StoreProcedures.GetEmail;
                listDB = db.Database.SqlQuery<Mail>(strSql).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listDB;
        }

        public long InsertMail(Mail mail)
        {
            db.Mails.Add(mail);
            db.SaveChanges();
            return mail.MailId;
        }
    }
}
